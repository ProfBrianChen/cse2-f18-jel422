// Jason Lee | CSE 002 Section 110 | 9/18/18 | Homework 03: Pyramid

import java.util.Scanner; // Imports the Scanner class for future use in code

public class Pyramid { // Main method required for every Java program
	public static void main(String[] args) { // Creates a class for the Java program
		Scanner myScanner = new Scanner(System.in);
			// Declares an instance of a Scanner object that takes input
		System.out.print("The square side of the pyramid is: ");
			// Prompts the user to input the square side of the pyramid
		double squareSide = myScanner.nextDouble();
			// Sets the next double the user inputs (which should be the length of the square side) to the variable squareSide
		System.out.print("The height of the pyramid is: ");
			// Prompts the user to input the height of the pyramid
		double pyramidHeight = myScanner.nextDouble();
			// Sets the next double the user inputs (which should be the height of the pyramid) to the variable pyramidHeight
		
		double pyramidVolume = Math.pow(squareSide, 2) * pyramidHeight / 3;
			// Calculates the volume of the pyramid with the given dimensions (volume of a square pyramid is b^2 * h / 3)
		
		System.out.println("The volume inside the pyramid is: " + pyramidVolume);
			// Prints the volume of the pyramid based on the dimensions the user inputted
	} // End of class
} // End of main method