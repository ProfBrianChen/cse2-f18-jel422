// Jason Lee | CSE 002 Section 110 | 9/18/18 | Homework 03: Convert

import java.util.Scanner; // Imports the Scanner class for future use in code

public class Convert { // Main method required for every Java program
	public static void main(String[] args) { // Creates a class for the Java program
		Scanner myScanner = new Scanner(System.in);
			// Declares an instance of a Scanner object that takes input
		System.out.print("Enter the affected area in acres: ");
		 // Prompts the user to input the affected area of the hurricane
		double affectedAreaAcres = myScanner.nextDouble();
			// Sets the next double the user inputs (which should be the affected area in acres) to the variable affectedAreaAcres
		System.out.print("Enter the rainfall in the affected area in inches: ");
			// Prompts the user to input the average rainfall in inches
		double averageRainfallInches = myScanner.nextDouble();
			// Sets the next double the user inputs (which should be the average rainfall) to the variable averageRainfallInches
		
		double affectedAreaMiles = affectedAreaAcres / 640;
			// Converts the affected area from acres to square miles (there are 640 acres in a square mile)
		double averageRainfallMiles = averageRainfallInches / 63360;
			// Converts the average rainfall from inches to miles (there are 63,360 inches in a mile)
		double rainVolume = affectedAreaMiles * averageRainfallMiles;
			// Calculates the total volume of rain in cubic miles based ono the affected area and average amount of rainfall
		
		System.out.println("The hurricane produced " + rainVolume + " cubic miles of rain.");
			// Prints the total volume of rain the hurricane produced in cubic miles
	} // End of class
} // End of main method