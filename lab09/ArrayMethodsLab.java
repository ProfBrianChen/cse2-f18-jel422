// Jason Lee | CSE 002 Section 110 | 11/9/18 | Lab 9: Passing Arrays Inside Methods

/* Purpose of code:
  To create methods which manipulate arrays through copying, reversing, printing, etc.
 */

import java.util.Arrays; // Imports the arrays class for future use in code

public class ArrayMethodsLab { // Creates a class for the Java program
   public static void main(String[] args) { // Main method required in every Java program
      int[] array0 = {2, 4, 6, 8, 10, 12, 14, 16}; // Creates a literal int[] array
      int[] array1 = copy(array0); // Uses copy() to duplicate array0
      int[] array2 = copy(array0); // Uses copy() to duplicate array0
      
      inverter(array0); // Inverts array0 using inverter()
      print(array0); // Prints array0
      
      inverter2(array1); // Inverts array2 (doesn't do anything because we returned a value and didn't affect the original list)
      print(array1); // Prints array1 (should be the same as the original array)
      
      int[] array3 = inverter2(array2); // Inverts array2 (this time it does something because the returned value is being assigned to a variable)
      print(array3); // Prints array3
   } // End of main method
   
   public static int[] copy(int[] list) { // Start of copy() method that returns type int[]
      int[] duplicate = new int[list.length]; // Creates an array with length list.length
      
      for (int i = 0; i < duplicate.length; i++) { // Start of for loop that runs list.length times
         duplicate[i] = list[i]; // Assigns each element in duplicate[] with the corresponding element in list[]
      } // End of for loop
      
      return duplicate; // Returns the duplicated array
   } // End of copy() method
   
   public static void inverter(int[] list) { // Start of inverter() method with no return value
      int temp; // Declares a variable to hold temporary values (needed for swapping)
      
      for (int i = 0; i < (list.length/2); i++) { // Start of for loop that runs list.length/2 times
         temp = list[i]; // Sets temp to the ith element of list[]
         list[i] = list[list.length - i - 1]; // Sets the ith element of list[] to the ith to last element of list[]
         list[list.length - i - 1] = temp; // Sets the ith to last element of list[] to the temp value (the original value of the ith element of list[])
      } // End of for loop
   } // End of inverter() method
   
   public static int[] inverter2(int[] list) { // Start of inverter2() method that returns type int[]
      int[] duplicate = copy(list); // Creates an array that is a duplicate of list
      inverter(duplicate); // Reverses the order of the duplicated array
      
      return duplicate; // Returns the new array (now duplicated and reversed)
   } // End of inverter2() method
   
   public static void print(int[] list) { // Start of print() method with no return value
      for (int i = 0; i < (list.length - 1); i++) { // Start of for loop that runs list.length times
         System.out.print(list[i] + ", "); // Prints every element except for the last one with a comma and space in front
      } // End of for loop
      System.out.println(list[list.length - 1]); // Prints the last element without a comma and followed by a new line
   } // End of print() method
} // End of class