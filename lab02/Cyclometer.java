// Jason Lee | CSE 002 Section 110 | 9/7/18 | Lab 02: Arithmetic Calculations

/* Purpose of code:
For two trips (given the time elapsed in seconds and the number of rotations), this program prints:
The number of minutes for each trip
The number of counts for each trip
The distance of each trip in miles
The distance for the two trips combined */

public class Cyclometer { // Main method required for every Java program
	public static void main(String[] args) { // Creates a class for the Java program
		int secsTrip1 = 480; // Defines and assigns values to the variable for the time elapsed in seconds of trip 1
		int secsTrip2 = 3220; // Defines and assigns values to the variable for the time elapsed in seconds of trip 2
		int countsTrip1 = 1561; // Defines and assigns values to the variable for the number of rotations in trip 1
		int countsTrip2 = 9037; // Defines and assigns values to the variable for the number of rotations in trip 2
		
		double wheelDiameter = 27.0; // Defines and assigns the diameter of the wheel as a variable
		double PI = 3.14159; // Defines and assigns the value of pi as a variable
		double feetPerMile = 5280; // Defines and assigns how many feet are in a mile as a variable
		double inchesPerFoot = 12; // Defines and assigns how many inches are in a foot as a variable
		double secondsPerMinute = 60; // Defines and assigns how many seconds are in a minute as a variable
		double distanceTrip1, distanceTrip2, totalDistance; // Defines variables for later use
		
		System.out.println("Trip 1 took " + (secsTrip1 / secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
			// Prints how long trip 1 took and the amount of rotations the wheel had in trip 1
		System.out.println("Trip 2 took " + (secsTrip2 / secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
			// Prints how long trip 2 took and the amount of rotations the wheel had in trip 2
		
		distanceTrip1 = countsTrip1 * wheelDiameter * PI;
			// Gives the distance the wheel traveled on trip 1 in inches (circumference multiplied by number of rotations)
		distanceTrip1 /= inchesPerFoot * feetPerMile;
			// Converts the value set for distanceTrip1 from inches to miles
		distanceTrip2 = countsTrip2 * wheelDiameter * PI;
			// Gives the distance the wheel traveled on trip 2 in inches (circumference multiplied by number of rotations)
		distanceTrip2 /= inchesPerFoot * feetPerMile;
			// Converts the value set for distanceTrip2 from inches to miles
		totalDistance = distanceTrip1 + distanceTrip2;
			// Sums the distance traveled in miles on trip 1 and trip 2
		
		System.out.println("Trip 1 was " + distanceTrip1 + " miles");
			// Prints the distance traveled on trip 1
		System.out.println("Trip 2 was " + distanceTrip2 + " miles");
			// Prints the distance traveled on trip 2
		System.out.println("The total distance was " + totalDistance + " miles");
			// Prints the total distance traveled on trip 1 and trip 2 combined
	} // End of main method
} // End of class