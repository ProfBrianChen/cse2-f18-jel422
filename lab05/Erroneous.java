// Jason Lee | CSE 002 Section 110 | 10/5/18 | Lab 05: Reading Erroneous Input

/* Purpose of code:
Asks the user for information regarding a course they are taking, and checks if the information they input is valid*/

import java.util.Scanner; // Imports the Scanner class for future use in code

public class Erroneous { // Creates a class for the Java program
	public static void main(String[] args) { // Main method required in every Java program
		Scanner myScanner = new Scanner(System.in);
			// Declares an instance of a Scanner object that takes input
		boolean intCheck, stringCheck;
			// Declares variables to check if the input was the correct type for later use in code
		int courseNum, meetFrequency, startTime, numStudents;
			// Declares integer variables to hold the values for each piece of information for later use in code
		String deptName, instructorName;
			// Declares string variables to hold the values for each piece of information for later use in code
		
		System.out.print("Enter the couse number: "); // Prompts the user for their course number
		intCheck = myScanner.hasNextInt(); // Checks if the input was an integer
		while (!intCheck) { // If the input was not an integer
			myScanner.next(); // Move to the next item in the conveyer belt
			System.out.print("An invalid course number was inputted. Try again: "); // Tells the user the input was invalid and takes new input
			intCheck = myScanner.hasNextInt(); // Checks if the input was an integer
		} // Loops back up with the new conditions
		courseNum = myScanner.nextInt(); // Sets the variable to the proper value
		
		System.out.print("Enter the department name: "); // Prompts the user for their department name
		stringCheck = myScanner.hasNext(); // Checks if the input was a string
		while (!stringCheck) { // If the input was not a string
			myScanner.next(); // Move to the next item in the conveyer belt
			System.out.print("An invalid department name was inputted. Try again: "); // Tells the user that the input was invalid and takes new input
			stringCheck = myScanner.hasNext(); // Checks if the input was a string
		} // Loops back up with the new conditions
		deptName = myScanner.next(); // Sets the variable to the proper value
		
		System.out.print("Enter the number of times your class meets in a week: "); // Prompts the user for the number of times their class meets
		intCheck = myScanner.hasNextInt(); // Checks if the input was an integer
		while (!intCheck) { // If the input was not an integer
			myScanner.next(); // Move to the next item in the conveyer belt
			System.out.print("An invalid number was inputted. Try again: "); // Tells the user the input was invalid and takes new input
			intCheck = myScanner.hasNextInt(); // Checks if the input was an integer
		} // Loops back up with the new conditions
		meetFrequency = myScanner.nextInt(); // Sets the variable to the proper value
		
		System.out.print("Enter the time your class starts: "); // Prompts the user for the time their class starts
		intCheck = myScanner.hasNextInt(); // Checks if the input was an integer
		while (!intCheck) { // If the input was not an integer
			myScanner.next(); // Move to the next item in the conveyer belt
			System.out.print("An invalid time was inputted. Try again: "); // Tells the user the input was invalid and takes new input
			intCheck = myScanner.hasNextInt(); // Checks if the input was an integer
		} // Loops back up with the new conditions
		startTime = myScanner.nextInt(); // Sets the variable to the proper value
		
		System.out.print("Enter the instructor name: "); // Prompts the user for their instructor name
		stringCheck = myScanner.hasNext(); // Checks if the input was a string
		while (!stringCheck) { // If the input was not a string
			myScanner.next(); // Move to the next item in the conveyer belt
			System.out.print("An invalid instructor name was inputted. Try again: "); // Tells the user the input was invalid and takes new input
			stringCheck = myScanner.hasNext(); // Checks if the input was a string
		} // Loops back up with the new conditions
		instructorName = myScanner.next(); // Sets the variable to the proper value
		
		System.out.print("Enter the number of students in your class: "); // Prompts the user for the number of students in their class
		intCheck = myScanner.hasNextInt(); // Checks if the input was an integer
		while (!intCheck) { // If the input was not an integer
			myScanner.next(); // Move to the next item in the conveyer belt
			System.out.print("An invalid student number was inputted. Try again: "); // Tells the user the input was invalid and takes new input
			intCheck = myScanner.hasNextInt(); // Checks if the input was an integer
		} // Loops back up with the new conditions
		numStudents = myScanner.nextInt(); // Sets the variable to the proper value
	} // End of main method
} // End of class