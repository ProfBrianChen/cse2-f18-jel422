// Jason Lee | CSE 002 Section 110 | 10/30/18 | Homework 07: Word Check

import java.util.Scanner; // Imports the Scanner class for future use in code

public class Hw07 { // Creates a class for the Java program
   public static void main(String[] args) { // Main method required for every Java program
      Scanner myScanner = new Scanner(System.in); // Creates a scanner object to receive input for the user's input
      String currentOption, userText, stringReturn, stringToFind; // Declares various string variables required for later use in the main method
      int intReturn; // Declares an int variable required for later use in the main method
      
      userText = sampleText(myScanner); // Sets variable userText to an input the user enters through the sampleText method
      while (true) { // Runs the program over and over until the user enters "q"  to quit
         currentOption = printMenu(myScanner); // Displays the menu to the user and sets currentOption to the option the user's input (checking if the input is correct is done inside of the method)
         while (!currentOption.equals("c") && !currentOption.equals("w") && !currentOption.equals("f") && !currentOption.equals("r") && !currentOption.equals("s") && !currentOption.equals("q")) { // Repeats this loop until the user inputs a correct option
            System.out.println ("An invalid option was entered. Please use one of the choices from the menu below."); // Tells the user that their input was incorrect, and asks them to try again
            currentOption = printMenu(myScanner); // Run the printMenu method again to display the menu again and receive another input
         } // End of while loop

         if (currentOption.equals("c")) { // If the user chose to see the number of non-whitespace characters
            intReturn = getNumOfNonWSCharacters(userText); // Sets intReturn to the value getNumOfNonWSCharacters returns
            System.out.println("Number of non-whitespace characters: " + intReturn); // Displays the number of non-whitespace characters to the user
         } // End of if statement
         
         else if (currentOption.equals("w")) { // If the user chose to see the number of words
            intReturn = getNumOfWords(userText); // Sets intReturn to the value getNumOfWords returns
            System.out.println("Number of words: " + intReturn); // Displays the number of words to the user
         } // End of if statement
         
         else if (currentOption.equals("f")) { // If the user chose to find a specific word or phrase inside of the entire string
            System.out.print("Enter a word or phrase to be found: "); // Prompts the user to input the word or phrase they want to find
            stringToFind = myScanner.nextLine(); // Uses scanner to take input for the phrase
            intReturn = findText(userText, stringToFind); // Sets intReturn to the value findText returns
            System.out.println("\"" + stringToFind + "\" instances: " + intReturn); // Displays the number of instances of the user's chosen string
         } // End of if statement
         
         else if (currentOption.equals("r")) { // If the user chose to replace all exclamation points with periods
            stringReturn = replaceExclamation(userText); // Sets stringReturn to the value replaceExclamation returns
            System.out.println("Edited text: " + stringReturn); // Displays the edited string to the user
         } // End of if statement
         
         else if (currentOption.equals("s")) { // If the user chose to shorten all instances where there are 2 or more successive spaces to one space
            stringReturn = shortenSpace(userText); // Sets stringReturn to the value shortenSpace returns
            System.out.println("Edited text: " + stringReturn); // Displays the edited string to the user
         } // End of if statement
         
         else if (currentOption.equals("q")) { // If the user chose to quit the program
            break; // Breaks out of the infinite loop, reaching the end of the program
         } // End of if statement
      } // End of infinite loop
      
      System.out.println("Successfully quit."); // Displays that the user has quit the program
   } // End of main method
   
   public static String sampleText(Scanner myScanner) { // Begins method for user to input values
      System.out.print("Enter a sample text: "); // Prompts the user to input a string
      String userInput = myScanner.nextLine(); // Scanner takes the user's input
      System.out.println("You entered: " + userInput); // Displays the user's input back to the user
      
      return userInput; // Returns the user's input
   } // End of sampleText method
   
   public static String printMenu(Scanner myScanner) { // Begins method to display the menu and take the user's choice
      System.out.println(); // Displays empty line so there isn't a clumped appearance with the last line in the output
      System.out.println("MENU"); // Lines 64-72 displays the menu...
      System.out.println("c - Number of non-whitespace characters");
      System.out.println("w - Number of words");
      System.out.println("f - Find text");
      System.out.println("r - Replace all !'s");
      System.out.println("s - Shorten spaces");
      System.out.println("q - Quit");
      System.out.println();
      System.out.print("Choose an option: "); // Prompts the user to choose an option from the menu
      String userInput = myScanner.nextLine(); // Scanner takes input from the user
      
      return userInput; // Returns the user's choice
   } // End of printMenu method
   
   public static int getNumOfNonWSCharacters(String userText) { // Begins method to find the number of non-whitespace characters
      int numChars = 0; // Declares a variable for the number of non-whitespace characters
      for (int i = 0; i < userText.length(); i++) { // Creates a for loop to loop through each character
         if (!Character.isWhitespace(userText.charAt(i))) { // If the current character is not a whitespace character
            numChars++; // Increase the counter by one
         } // End of if statement
      } // End of for loop
      
      return numChars; // Returns the number of non-whitespace characters
   } // End of getNumOfNonWSCharacters method
   
   public static int getNumOfWords(String userText) { // Begins method to find the number of words
      int numWords = 0; // Declares a variable for the number of words
      boolean processingWord = false; // Creates a variable that keeps track of if you're looping through a word or not in the for loop
      
      for (int i = 0; i < userText.length(); i++) { // Start of for loop
         if (processingWord && Character.isWhitespace(userText.charAt(i))) { // If you are processing a word and you have just reached a whitespace character
            numWords++; // Increase the counter by one (because you just reached the end of a word)
            processingWord = false; // Set processingWord to false to signify you just reached the end of a word
         } // End of if statement
         else if (!processingWord && Character.isLetter(userText.charAt(i))) { // If you are not processing a word and you have just reached a letter
            processingWord = true; // Set processingWord to true to signify you just entered a word in the for loop
         } // End of if statement
         
         if (i == userText.length() - 1 && !Character.isWhitespace(userText.charAt(i))) { // If you reached the end of a word at the end of a string (because the if statements earlier won't detect a word at the end of the string)
            numWords++; // Increase the counter by one
            processingWord = false; // Set processingWord to false to signify you just reached the end of a word
         } // End of if statement
      } // End of for loop
      
      return numWords; // Returns the number of words in the string
   } // End of getNumOfWords method
   
   public static int findText(String userText, String stringToFind) { // Begins method to find a certain word or phrase within the entire string
      int numInstances = 0; // Declares a variable for the number of instances the word or phrase appears in the string
      
      for (int i = 0; i <= (userText.length() - stringToFind.length()); i++) { // Creates a for loop that repeats until you reach the point where the word or phrase wouldn't fit at the end of the string
         if (userText.substring(i, i + stringToFind.length()).equals(stringToFind)) { // If the substring at position i with the length of the word or phrase equals the word or phrase
            numInstances++; // Increase the counter by one
         } // End of if statement
      } // End of for loop
      
      return numInstances; // Returns the number of instances the word or phrase appears in the string
   } // End of findText method
   
   public static String replaceExclamation(String userText) { // Creates a method to replace all exclamation points with periods
      String editedText = ""; // Declares a variable to hold the value for the edited text
      
      for (int i = 0; i < userText.length(); i++) { // Creates a for loop that loops through each character in the string
         if (userText.charAt(i) == '!') { // If the character is an exclamation point
            editedText += "."; // Concatenate a period onto the end of the editedText variable
         } // End of if statement
         else { // If the character is not an exclamation point
            editedText += userText.charAt(i); // Append the original character onto the end of the editedText variable
         } // End of else statement
      } // End of for loop
      
      return editedText; // Returns the value of editedText
   } // End of replaceExclamation method
   
   public static String shortenSpace(String userText) { // Creates a method to shorten all spaces to one space
      String beforeEdited = userText; // Creates a variable to hold the value of the text before the last edit
      String afterEdited = beforeEdited.replace("  ", " "); // Creates a variable to hold the value of the text after the edit
      
      while (beforeEdited != afterEdited) { // Repeat until the text before the edit is the same as the text after the edit
         beforeEdited = afterEdited; // Sets beforeEdited to the most recent edit
         afterEdited = beforeEdited.replace("  ", " "); // Edits the text and sets it to afterEdited
      } // End of while loop
      
      return afterEdited; // Returns the value of the text after it is fully edited
   } // End of shortenSpace method
} // End of class