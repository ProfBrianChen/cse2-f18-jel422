// Jason Lee | CSE 002 Section 110 | 9/21/18 | Lab 04: Card Generator

/* Purpose of code:
Pick a random card from a deck of 52 playing cards and displays it to the user */

public class CardGenerator { // Creates a class for the Java program
	public static void main(String[] args) { // Main method required for every Java program
		int chosenCard = (int) (Math.random() * 52) + 1; // Generates a random number representing a card
		int cardNumber = chosenCard % 13; // Calculates the actual value shown on the card
		String suit, identity; // Defines variables for the card's suit and identity for later use
		
		// The following if-else statements on lines 13-27 determine the suit of the card
		if ((1 <= chosenCard) && (chosenCard <= 13)) { // If the random number generated is 1-13, the suit is diamonds
			suit = "Diamonds";
		}
		else if ((14 <= chosenCard) && (chosenCard <= 26)) { // If the random number generated is 14-26, the suit is clubs
			suit = "Clubs";
		}
		else if ((27 <= chosenCard) && (chosenCard <= 39)) { // If the random number generated is 27-39, the suit is hearts
			suit = "Hearts";
		}
		else if ((40 <= chosenCard) && (chosenCard <= 52)) { // If the random number generated is 40-52, the suit is spades
			suit = "Spades";
		}
		else { // Should never run, but a safety statement if the random number is less than 1 or greater than 52
			suit = "INVALID SUIT";
		}
		
		// The following switch statement on lines 30-62 determine the identity of the card
		switch (cardNumber) {
			case 2: // If the actual value shown on the card is 2-10, the identity should be the number
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
			case 10:
				identity = Integer.toString(cardNumber);
				break;
				
			case 1: // If the actual value shown on the card is 1, the identity should be "Ace"
				identity = "Ace";
				break;
				
			case 11: // If the actual value shown on the card is 11, the identity should be "Jack"
				identity = "Jack";
				break;
				
			case 12: // If the actual value shown on the card is 12, the identity should be "Queen"
				identity = "Queen";
				break;
				
			case 0: // If the actual value shown on the card is 13 (13 mod 13 = 0), the identity should be "King"
				identity = "King";
				break;
				
			default: // Should never run, but a safety statement if the actual value calculated is less than 0 (13 in reality) or greater than 12
				identity = "INVALID IDENTITY";
				break;
		}
		
		System.out.println("You picked the " + identity + " of " + suit + "."); // Prints the final result
	} // End of mian method
} // End of class