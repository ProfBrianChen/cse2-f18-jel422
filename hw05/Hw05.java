// Jason Lee | CSE 002 Section 110 | 10/9/18 | Homework 05: Poker

import java.util.Scanner; // Imports the Scanner class for future use in code

public class Hw05 { // Creates a class for the Java program
   public static void main(String[] args) { // Main method required for every Java program
      int numHands, cardOne, cardTwo, cardThree, cardFour, cardFive; // Declares variables for the number of hands and the five cards
      int cardOneNum, cardTwoNum, cardThreeNum, cardFourNum, cardFiveNum; // Declares variables for the face (number) value of the five cards
      int onePairCount = 0, twoPairsCount = 0, threeOfKindCount = 0, fourOfKindCount = 0; // Declares variables for the number of each kind of hand we have
      double onePairChance, twoPairsChance, threeOfKindChance, fourOfKindChance; // Declares variables for the calculated percentage of each hand in the total number of hands drawn
      boolean intCheck; // Declares a variable to check if the user inputted an integer when inputting the number of hands
      Scanner myScanner = new Scanner(System.in); // Creates a scanner object to receive input for the user's input
      
      System.out.print("Enter the amount of hands to generate: "); // Prompts the user to imput the amount of hands to draw
      intCheck = myScanner.hasNextInt(); // Checks if the value the user inputted is an integer
      while (!intCheck) { // Repeat while the user doesn't input an integer
         myScanner.next(); // Ignore the value the user inputted
         System.out.print("An invalid value was entered for the number of hands to generate. Try again: "); // Tells the user their input was invalid and prompts them to try again
         intCheck = myScanner.hasNextInt(); // Checks if the new value the user inputted is an integer
      }
      numHands = myScanner.nextInt(); // Sets the variable numHands to the integer the user inputted
      
      for (int i = 1; i <= numHands; i++) { // Creates a loop with numHands iterations
         cardOne = 0; cardTwo = 0; cardThree = 0; cardFour = 0; cardFive = 0; // Sets the values for the five cards to 0 at the start of each loop so they don't interfere with the upcoming do-while loops in future iterations
         
         cardOne = (int) ((Math.random() * 52) + 1); // Sets card one to a random integer 1-52
         do { cardTwo = (int) ((Math.random() * 52) + 1); } // Sets card two to a random integer 1-52 repeatedly until it doesn't equal card one
         while (cardTwo == cardOne);
         do { cardThree = (int) ((Math.random() * 52) + 1); } // Sets card three to a random integer 1-52 repeatedly until it doesn't equal card one or card two
         while ((cardThree == cardOne) || (cardThree == cardTwo));
         do { cardFour = (int) ((Math.random() * 52) + 1); } // Sets card four to a random integer 1-52 repeatedly until it doesn't equal card one, card two, or card three
         while ((cardFour == cardOne) || (cardFour == cardTwo) || (cardFour == cardThree));
         do { cardFive = (int) ((Math.random() * 52) + 1); } // Sets card five to a random integer 1-52 repeatedly until it doesn't equal card one, card two, card three, or card four
         while ((cardFive == cardOne) || (cardFive == cardTwo) || (cardFive == cardThree) || (cardFive == cardFour));
         
         cardOneNum = cardOne % 13; // Calculates the face (number) value of card one
         cardTwoNum = cardTwo % 13; // Calculates the face (number) value of card two
         cardThreeNum = cardThree % 13; // Calculates the face (number) value of card three
         cardFourNum = cardFour % 13; // Calculates the face (number) value of card four
         cardFiveNum = cardFive % 13; // Calculates the face (number) value of card five
         
         if (((cardOneNum == cardTwoNum) && (cardTwoNum == cardThreeNum) && (cardThreeNum == cardFourNum)) || ((cardOneNum == cardTwoNum) && (cardTwoNum == cardThreeNum) && (cardThreeNum == cardFiveNum)) || ((cardOneNum == cardTwoNum) && (cardTwoNum == cardFourNum) && (cardFourNum == cardFiveNum)) || ((cardOneNum == cardThreeNum) && (cardThreeNum == cardFourNum) && (cardFourNum == cardFiveNum)) || ((cardTwoNum == cardThreeNum) && (cardThreeNum == cardFourNum) && (cardFourNum == cardFiveNum))) {
            // Checks all possible combinations of the five cards having a four of a kind
            fourOfKindCount += 1; // Increase the "four of a kind" counter by one if there is a four of a kind
         }
         else if (((cardOneNum == cardTwoNum) && (cardTwoNum == cardThreeNum)) || ((cardOneNum == cardTwoNum) && (cardTwoNum == cardFourNum)) || ((cardOneNum == cardTwoNum) && (cardTwoNum == cardFiveNum)) || ((cardOneNum == cardThreeNum) && (cardThreeNum == cardFourNum)) || ((cardOneNum == cardThreeNum) && (cardThreeNum == cardFiveNum)) || ((cardOneNum == cardFourNum) && (cardFourNum == cardFiveNum)) || ((cardTwoNum == cardThreeNum) && (cardThreeNum == cardFourNum)) || ((cardTwoNum == cardThreeNum) && (cardThreeNum == cardFiveNum)) || ((cardTwoNum == cardFourNum) && (cardFourNum == cardFiveNum)) || ((cardThreeNum == cardFourNum) && (cardFourNum == cardFiveNum))) {
            // Checks all possible combinations of the five cards having a three of a kind knowing that there isn't a four of a kind
            threeOfKindCount += 1; // Increase the "three of a kind" counter by one if there is a three of a kind
         }
         else if (((cardOneNum == cardTwoNum) && (cardThreeNum == cardFourNum)) || ((cardOneNum == cardTwoNum) && (cardThreeNum == cardFiveNum)) || ((cardOneNum == cardTwoNum) && (cardFourNum == cardFiveNum)) || ((cardOneNum == cardThreeNum) && (cardTwoNum == cardFourNum)) || ((cardOneNum == cardThreeNum) && (cardTwoNum == cardFiveNum)) || ((cardOneNum == cardThreeNum) && (cardFourNum == cardFiveNum)) || ((cardOneNum == cardFourNum) && (cardTwoNum == cardThreeNum)) || ((cardOneNum == cardFourNum) && (cardTwoNum == cardFiveNum)) || ((cardOneNum == cardFourNum) && (cardThreeNum == cardFiveNum)) || ((cardOneNum == cardFiveNum) && (cardTwoNum == cardThreeNum)) || ((cardOneNum == cardFiveNum) && (cardTwoNum == cardFourNum)) || ((cardOneNum == cardFiveNum) && (cardThreeNum == cardFourNum)) || ((cardTwoNum == cardThreeNum) && (cardFourNum == cardFiveNum)) || ((cardTwoNum == cardFourNum) && (cardThreeNum == cardFiveNum)) || ((cardTwoNum == cardFiveNum) && (cardThreeNum == cardFourNum))) {
            // Checks all possible permutations of the five cards having two pairs knowing that there isn't a four of a kind or a three of a kind
            twoPairsCount += 1; // Increase the "two pairs" counter by one if there are two pairs
         }
         else if ((cardOneNum == cardTwoNum) || (cardOneNum == cardThreeNum) || (cardOneNum == cardFourNum) || (cardOneNum == cardFiveNum) || (cardTwoNum == cardThreeNum) || (cardTwoNum == cardFourNum) || (cardTwoNum == cardFiveNum) || (cardThreeNum == cardFourNum) || (cardThreeNum == cardFiveNum) || (cardFourNum == cardFiveNum)) {
            // Checks all possible combinations of the five cards having one pair knowing that there isn't a four of a kind, a three of a kind, or two pairs
            onePairCount += 1; // Increases the "one pair" counter by one if there is one pair
         }
      }
      
      onePairChance = ((double) (onePairCount) / (numHands)) * 100; // Calculates the percentage of "one pair" hands by converting the counter to a double, dividing it by the total number of hands, and multiplying by 100
      twoPairsChance = ((double) (twoPairsCount) / (numHands)) * 100; // Calculates the percentage of "two pairs" hands by converting the counter to a double, dividing it by the total number of hands, and multiplying by 100
      threeOfKindChance = ((double) (threeOfKindCount) / (numHands)) * 100; // Calculates the percentage of "three of a kind" hands by converting the counter to a double, dividing it by the total number of hands, and multiplying by 100
      fourOfKindChance = ((double) (fourOfKindCount) / (numHands)) * 100; // Calculates the percentage of "four of a kind" hands by converting the counter to a double, dividing it by the total number of hands, and multiplying by 100
      
      System.out.println("Number of hands chosen: " + numHands); // Displays to the user the amount of hands drawn
      System.out.printf("The probability of one pair: %6.3f percent\n", onePairChance); // Displays to the user the percentage of "one pair" hands in their total hands drawn
      System.out.printf("The probability of two pairs: %6.3f percent\n", twoPairsChance); // Displays to the user the percentage of "two pairs" hands in their total hands drawn
      System.out.printf("The probability of three of a kind: %6.3f percent\n", threeOfKindChance); // Displays to the user the percentage of "three of a kind" hands in their total hands drawn
      System.out.printf("The probability of four of a kind: %6.3f percent\n", fourOfKindChance); // Displays to the user the percentage of "four of a kind" hands in their total hands drawn
   } // End of main method
} // End of class