// CSE 002 Section 110 Homework 1: Welcome Message

public class WelcomeClass{
	
	public static void main(String args[]){
		// Prints a personalized welcome messageto the terminal window
		System.out.println("  -----------");
		System.out.println("  | WELCOME |");
		System.out.println("  -----------");
		System.out.println("  ^  ^  ^  ^  ^  ^");
		System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
		System.out.println("<-J--E--L--4--2--2->");
		System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / ");
		System.out.println("  v  v  v  v  v  v");
		System.out.println("Jason Lee is a first year student at Lehigh University. He plans to major in mathematics and minor in computer science.");
		
	}

}