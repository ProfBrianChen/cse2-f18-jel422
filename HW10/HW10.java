// Jason Lee | CSE 002 Section 110 | 12/4/18 | Homework 10: Tic-Tac-Toe

import java.util.Scanner; // Imports the scanner class for future use in code

public class HW10 { // Creates a class for the Java program
   public static void main(String[] args) { // Main method required in every Java program
      Scanner myScanner = new Scanner(System.in); // Creates a scanner object to take input from the user
      char currentPlayer = 'O', outcome = '0'; // Creates variables for whoever's turn it is and the current state of the outcome of the game
      char[][] gameBoard = { // Creates a two-dimensional array holding the values for the board
         {'1', '2', '3'},
         {'4', '5', '6'},
         {'7', '8', '9'}
      };
      
      while (outcome == '0') { // Repeats until an outcome has been reached
         displayBoard(gameBoard); // Displays the board
         System.out.print("It is player " + currentPlayer + "'s turn. Enter the tile for where you want to place your token: "); // Prints who's turn it is, and prompts for a tile to be entered
         char position = getInput(gameBoard, myScanner); // Runs the method getInput() and sets the returned value to position
         gameBoard[(position - 1) / 3][(position - 1) % 3] = currentPlayer; // Sets the corresponding array position of the user's input to the current player's token
            // Mathematically, this works because the decimals of the result of (position - 1) / 3 will be truncated (integer division), always giving the correct row
            // (position - 1) mod 3 works for the columns mathemtatically because the remainder is how much you move to the right by
         if (currentPlayer == 'O') { // If it was O's turn...
            currentPlayer = 'X'; // Make it X's turn now
         } // End of if statement
         else if (currentPlayer == 'X') { // If it was X's turn...
            currentPlayer = 'O'; // Make it O's turn now
         } // End of if statement
         outcome = checkOutcome(gameBoard); // Sets outcome to the current outcome of the gameboard
      } // End of while loop
      
      displayBoard(gameBoard); // Displays the board
      if (outcome == 'O' || outcome == 'X') { // If the outcome value is 'X' or 'Y'...
         System.out.println("Player " + outcome + " has won!"); // Prints a statement saying that player has won
      } // End of if statement
      else if (outcome == 'T') { // Otherwise, if the outcome value is 'T'...
         System.out.println("There was a tie!"); // Prints a statement saying there was a tie
      } // End of else-if statement
   } // End of main method
   
   public static void displayBoard(char[][] gameBoard) { // Start of displayBoard() method
      for (int i = 0; i < 3; i++) { // Loops through the rows of the board
         for (int j = 0; j < 3; j++) { // Loops through the columns of each row of the board
            System.out.print(gameBoard[i][j] + " "); // Prints the value of the index at the current iteration followed by a space
         } // End of inner for loop
         System.out.println(); // Prints a new line (we are at the end of a row)
      } // End of outer for loop
   } // End of displayBoard() method
   
   public static char getInput(char[][] gameBoard, Scanner myScanner) { // Start of getInput() method
      int returnValue; // Declares a variable for the value that will be returned
      boolean intCheck; // Declares a variable to check if the user's input was an integer or not
      
      while (true) { // Repeats infinitely (but note the return statement within the loop...)
         intCheck = myScanner.hasNextInt(); // Sets intCheck to determine if the user's input was an integer or not
         if (intCheck) { // If the input was an integer...
            returnValue = myScanner.nextInt(); // Sets the variable returnValue to that integer
            if (returnValue >= 1 && returnValue <= 9) { // If the input is within the range 1 - 9 inclusive...
               if (gameBoard[(returnValue - 1) / 3][(returnValue - 1) % 3] != 'O' && gameBoard[(returnValue - 1) / 3][(returnValue - 1) % 3] != 'X') {
                  // If the tile the user entered is not already occupied by another token (read reasoning for mathematics on lines 20 - 21 of code)...
                  return (char)(returnValue); // Returns the returnValue (but as a char since the gameBoard's array type is char), thereby ending execution of the method
               } // End of if statement
               else { // If the user entered a tile that was already occupied...
                  System.out.print("A tile that is already occupied was entered. Try again: "); // Prints an error message and prompts the user for another tile
               } // End of else statement
            } // End of if statement
            else { // If the integer the user entered is not within the range 1 - 9 inclusive...
               System.out.print("A value out of the range of the game board (1 - 9) was entered. Try again: "); // Prints an error message and prompts the user for another input
            } // End of else statement
         } // End of if statement
         else { // If the input the user entered is not an integer...
            myScanner.next(); // Takes the user's last input off of the scanner
            System.out.print("An integer was not entered. Try again: "); // Prints an error message and promopts the user for another input
         } // End of else statement
      } // End of while loop
   } // End of getInput() method
   
   public static char checkOutcome(char[][] gameBoard) { // Start of checkOutcome() method
      if ( // This if statement checks all possibilities of a player winning that involves the use of the middle tile
          (gameBoard[1][1] == gameBoard[0][0] && gameBoard[1][1] == gameBoard[2][2]) || // If the top left, middle, and bottom right tiles are the same (diagonal)...
          (gameBoard[1][1] == gameBoard[0][2] && gameBoard[1][1] == gameBoard[2][0]) || // Or if the top right, middle, and bottom left tiles are the same (diagonal)...
          (gameBoard[1][1] == gameBoard[0][1] && gameBoard[1][1] == gameBoard[2][1]) || // Or if the top, middle, and bottom tiles are the same (vertical)...
          (gameBoard[1][1] == gameBoard[1][0] && gameBoard[1][1] == gameBoard[1][2]) // Or if the left, middle, and right tiles are the same (horizontal)...
         ) {
         return gameBoard[1][1]; // Return the value of the middle tile (will be the correct player since we checked for all win conditions only involving the middle tile)
      } // End of if statement
      else if ( // This else-if statement checks all possibilities of a player winning that involves the use of the top left tile that does not involve the middle tile
               (gameBoard[0][0] == gameBoard[0][1] && gameBoard[0][0] == gameBoard[0][2]) || // If the top left, top, and top right tiles are the same (horizontal on top)...
               (gameBoard[0][0] == gameBoard[1][0] && gameBoard[0][0] == gameBoard[2][0]) // Or if the top left, left, and bottom left tiles are the same (vertical on left)...
              ) {
         return gameBoard[0][0]; // Return the value of the top left tile (the winner)
      } // End of else-if statement
      else if ( // This else-if statement checks all possibilities of a player winning tha tinvolves the use of the bottom right tile that does not involve the middle tile
               (gameBoard[2][2] == gameBoard[2][1] && gameBoard[2][2] == gameBoard[2][0]) || // If the bottom right, right, and top right tiles are the same (vertical on right)
               (gameBoard[2][2] == gameBoard[1][2] && gameBoard[2][2] == gameBoard[0][2]) // Or if the bottom right, bottom, and bottom left tiles are the same (horizontal on bottom)...
              ) {
         return gameBoard[2][2]; // Return the value of the bottom left tile (the winner)
      } // End of else-if statement
      else if ( // This else-if statement checks if a tie has occurred (all spaces are occupied without any win conditions occurring)
               (gameBoard[0][0] == 'O' || gameBoard[0][0] == 'X') && 
               (gameBoard[0][1] == 'O' || gameBoard[0][1] == 'X') &&
               (gameBoard[0][2] == 'O' || gameBoard[0][2] == 'X') &&
               (gameBoard[1][0] == 'O' || gameBoard[1][0] == 'X') &&
               (gameBoard[1][1] == 'O' || gameBoard[1][1] == 'X') &&
               (gameBoard[1][2] == 'O' || gameBoard[1][2] == 'X') &&
               (gameBoard[2][0] == 'O' || gameBoard[2][0] == 'X') &&
               (gameBoard[2][1] == 'O' || gameBoard[2][1] == 'X') &&
               (gameBoard[2][2] == 'O' || gameBoard[2][2] == 'X')
              ) {
         return 'T'; // Returns the char 'T' indicating "tie"
      } // End of else-if statement
      else { // If no win conditions occurred and there is still at least 1 available space...
         return '0'; // Returns the char '0' indicating the game is still going on
      } // End of else statement
   } // End of checkOutcome() method
} // End of class