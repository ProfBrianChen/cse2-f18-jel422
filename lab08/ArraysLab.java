// Jason Lee | CSE 002 Section 110 | 11/9/18 | Lab 8: Arrays

/* Purpose of code:
  Determine the number of times integers 0 - 99 appears in an array
 */

import java.util.Scanner; // Imports the scanner class for future use in code
import java.util.Arrays; // Imports the arrays class for future use in code

public class ArraysLab { // Creates a class for the Java program
   public static void main(String[] args) { // Main method required in every Java program
      int[] numArray1 = new int[100]; // Creates an array with size 100
      int[] numArray2 = new int[100]; // Creates another array with size 100
      int i, j; // Declares variables for counters in for loops
      
      for (i = 0; i <= 99; i++) { // Repeats 100 times
         numArray1[i] = (int)(Math.random() * 100); // Sets the ith element of numArray1 to a random integer 0 - 99
      } // End of for loop
      
      System.out.println(Arrays.toString(numArray1)); // Prints the elements of numArray1
      
      for (i = 0; i <= 99; i++) { // Repeats 100 times
         int counter = 0; // Declares a variable to hold the counter for the amount of times a number appears
         for (j = 0; j <= 99; j++) { // Repeats 100 times
            if (i == numArray1[j]) { // If i (in this case, the index we are viewing in numArray2) is equal to the jth element in numArray 1
               counter++; // Increment the counter
            } // End of if statement
         } // End of for loop
         numArray2[i] = counter; // Sets the ith element of numArray2 to the amount of times that index appeared in numArray1
         if (counter == 1) { // If there was one element with that index
            System.out.println(i + " occurs " + counter + " time"); // Print a statement saying that index appeared once (singular)
         } // End of if statement
         else { // If that index appeared any amount of times except one
            System.out.println(i + " occurs " + counter + " times"); // Print a statement saying that index appeared counter times (plural)
         } // End of else statement
      } // End of for loop
   } // End of main method
} // End of class