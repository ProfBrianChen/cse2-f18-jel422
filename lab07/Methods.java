// Jason Lee | CSE 002 Section 110 | 10/26/18 | Lab 7: Methods

/* Purpose of code:
 Use methods to generate random types of sentences, generating a story.
 */

import java.util.Random; // Imports the random class for future use in code
import java.util.Scanner; // Imports the scanner class for future use in code

public class Methods { // Creates a class for the Java program
   public static void main(String[] args) { // Main method required in every Java program
      Random randomGenerator = new Random(); // Creates a Random object to generate random integers for later in code
      Scanner myScanner = new Scanner(System.in); // Creates a Scanner object to receive input from the user
      String sentence, subjectNoun, lastAdjective, currentAdjective; // Declares string variables for later use in code
      int generateNew; // Declares an int variable that holds the value for if a user would like to generate another sentence for phase 1
      boolean intCheck; // Declares a boolean variable to hold the value for if the user inputted an integer or not
      
      while (true) { // Creates an infinite loop
         sentence = "The "; // Starts off the first sentence
         lastAdjective = ""; // Sets lastAdjective to hold the first adjective so we can make sure the second adjective (if it exists) doesn't equal the first adjective
         for (int i = 0; i < randomGenerator.nextInt(3); i++) { // Creates a for loop that iterates 0, 1, or 2 times
            do { currentAdjective = generateAdjective(randomGenerator); } // Keeps generating new adjectives...
            while (currentAdjective.equals(lastAdjective)); // until it doesn't equal the first adjective (only possible on 2nd iteration)
            sentence += currentAdjective + " "; // Concatenates the adjectives onto the current sentence
            lastAdjective = currentAdjective; // Set lastAdjective to the last adjective we used
         } // End of for loop
         subjectNoun = generateSubjectNoun(randomGenerator); // Sets subjectNoun to a random subject noun from our method
         sentence += subjectNoun + " " + generateVerb(randomGenerator) + " the "; // Concatenates the subject noun and a random verb onto the sentence
         lastAdjective = ""; // Sets lastAdjective to hold the first adjective so we can make sure the second adjective (if it exists) doesn't equal the first adjective
         for (int i = 0; i < randomGenerator.nextInt(3); i++) { // Creates a for loop that iterates 0, 1, or 2 times
            do { currentAdjective = generateAdjective(randomGenerator); } // Keep generating new adjectives...
            while (currentAdjective.equals(lastAdjective)); // until it doesn't equal the first adjective (only possible on 2nd iteration)
            sentence += currentAdjective + " "; // Concatenates the adjectives onto the current sentence
            lastAdjective = currentAdjective; // Set lastAdjective to the last adjective we used
         } // End of for loop
         sentence += generateObjectNoun(randomGenerator) + "."; // Concatenates a random object noun to the sentence and puts a period to end the sentence.
         System.out.println(sentence); // Displays the full sentence
         
         System.out.print("Enter 1 if you would like to generate another sentence. Otherwise, enter 2: "); // Prompts the user to input if they want to generate another sentence or not
         intCheck = myScanner.hasNextInt(); // Sets intCheck to whether the user entered an integer or not
         while (true) { // Creates an infinite loop
            if (intCheck) { // If the user inputted an integer
               generateNew = myScanner.nextInt(); // Set generateNew to the value the user entered
               if (generateNew != 1 && generateNew != 2) { // If the value isn't 1 or 2...
                  System.out.print("An invalid value was entered. Try again: "); // Prompts the user to input another value because their original value was invalid
                  intCheck = myScanner.hasNextInt(); // Checks if the value the user inputted is an integer
               } // End of inner if statement
               else { // If the value is either 1 or 2...
                  break; // Break out of the while loop and proceed with the rest of the code
               } // End of inner else statement
            } // End of outer if statement
            else { // If the user inputted anything that is not an integer
               myScanner.next(); // Take that value off of the scanner's current value
               System.out.print("An invalid value was entered. Try again: "); // Prompts the user to input another value because their original value was invalid
               intCheck = myScanner.hasNextInt(); // Checks if the value the user inputted is an integer
            } // End of outer else statement
         } // End of infinite loop
         if (generateNew == 2) { // If the user chose to stick with the current sentence
            break; // Break out of the loop to continue with the code
         } // End of if statement
      } // End of infinite loop (end of phase 1)
      
      generateParagraph(randomGenerator, sentence, subjectNoun); // Generates a paragraph (phase 2)
   } // End of main method
   
   public static String generateAdjective(Random randomGenerator) { // Creates a method to pick a random adjective
      int randomInt = randomGenerator.nextInt(10); // Picks a random integer from 0 - 9
      String returnValue; // What the method returns
      
      switch (randomInt) { // Creates a swtich statement for the random integer
         case 0: // If the random integer is 0
            returnValue = "gorgeous"; // Return "gorgeous"
            break;
         case 1: // If the random integer is 1
            returnValue = "burned"; // Return "burned"
            break;
         case 2: // If the random integer is 2
            returnValue = "saucy"; // Return "saucy"
            break;
         case 3: // If the random integer is 3
            returnValue = "radioactive"; // Return "radioactive"
            break;
         case 4: // If the random integer is 4
            returnValue = "flowery"; // Return "flowery"
            break;
         case 5: // If the random integer is 5
            returnValue = "frightening"; // Return "frightening"
            break;
         case 6: // If the random integer is 6
            returnValue = "delicious"; // Return "delicious"
            break;
         case 7: // If the random integer is 7
            returnValue = "spicy"; // Return "spicy"
            break;
         case 8: // If the random integer is 8
            returnValue = "nifty"; // Return "nifty"
            break;
         case 9: // If the random integer is 9
            returnValue = "promiscuous"; // Return "promiscuous"
            break;
         default: // Default case for switch statement
            returnValue = "wishful"; // Return "wishful"
            break;
      } // End of switch statement
      
      return returnValue; // Returns returnValue
   } // End of generateAdjective method
   
   public static String generateSubjectNoun(Random randomGenerator) { // Creates a method to pick a random subject noun
      int randomInt = randomGenerator.nextInt(10); // Picks a random integer from 0 - 9
      String returnValue; // What the method returns
      
      switch (randomInt) { // Creates a swtich statement for the random integer
         case 0: // If the random integer is 0
            returnValue = "student"; // Return "student"
            break;
         case 1: // If the random integer is 1
            returnValue = "teacher"; // Return "teacher"
            break;
         case 2: // If the random integer is 2
            returnValue = "merchant"; // Return "merchant"
            break;
         case 3: // If the random integer is 3
            returnValue = "bear"; // Return "bear"
            break;
         case 4: // If the random integer is 4
            returnValue = "bird"; // Return "bird"
            break;
         case 5: // If the random integer is 5
            returnValue = "baby"; // Return "baby"
            break;
         case 6: // If the random integer is 6
            returnValue = "kitten"; // Return "kitten"
            break;
         case 7: // If the random integer is 7
            returnValue = "host"; // Return "host"
            break;
         case 8: // If the random integer is 8
            returnValue = "passenger"; // Return "passenger"
            break;
         case 9: // If the random integer is 9
            returnValue = "musician"; // Return "musician"
            break;
         default: // Default case for switch statement
            returnValue = "soldier"; // Return "soldier"
            break;
      } // End of switch statement
      
      return returnValue; // Returns returnValue
   } // End of generateSubjectNoun method
   
   public static String generateVerb(Random randomGenerator) { // Creates a method to pick a random verb
      int randomInt = randomGenerator.nextInt(10); // Picks a random integer from 0 - 9
      String returnValue; // What the method returns
      
      switch (randomInt) { // Creates a swtich statement for the random integer
         case 0: // If the random integer is 0
            returnValue = "damaged"; // Return "damaged"
            break;
         case 1: // If the random integer is 1
            returnValue = "possessed"; // Return "possessed"
            break;
         case 2: // If the random integer is 2
            returnValue = "grabbed"; // Return "grabbed"
            break;
         case 3: // If the random integer is 3
            returnValue = "bought"; // Return "bought"
            break;
         case 4: // If the random integer is 4
            returnValue = "took"; // Return "took"
            break;
         case 5: // If the random integer is 5
            returnValue = "finessed"; // Return "finessed"
            break;
         case 6: // If the random integer is 6
            returnValue = "ate"; // Return "ate"
            break;
         case 7: // If the random integer is 7
            returnValue = "greeted"; // Return "greeted"
            break;
         case 8: // If the random integer is 8
            returnValue = "healed"; // Return "healed"
            break;
         case 9: // If the random integer is 9
            returnValue = "hunted"; // Return "hunted"
            break;
         default: // Default case for switch statement
            returnValue = "warned"; // Return "warned"
            break;
      } // End of switch statement
      
      return returnValue; // Returns returnValue
   } // End of generateVerb method
   
   public static String generateObjectNoun(Random randomGenerator) { // Creates a method to pick a random object noun
      int randomInt = randomGenerator.nextInt(10); // Picks a random integer from 0 - 9
      String returnValue; // What the method returns
      
      switch (randomInt) { // Creates a swtich statement for the random integer
         case 0: // If the random integer is 0
            returnValue = "sushi"; // Return "sushi"
            break;
         case 1: // If the random integer is 1
            returnValue = "calculator"; // Return "calculator"
            break;
         case 2: // If the random integer is 2
            returnValue = "cell phone"; // Return "phone"
            break;
         case 3: // If the random integer is 3
            returnValue = "barrel"; // Return "leaf"
            break;
         case 4: // If the random integer is 4
            returnValue = "spaceship"; // Return "spaceship"
            break;
         case 5: // If the random integer is 5
            returnValue = "skateboard"; // Return "skateboard"
            break;
         case 6: // If the random integer is 6
            returnValue = "hoodie"; // Return "fish"
            break;
         case 7: // If the random integer is 7
            returnValue = "piano"; // Return "piano"
            break;
         case 8: // If the random integer is 8
            returnValue = "baseball"; // Return "baseball"
            break;
         case 9: // If the random integer is 9
            returnValue = "bottle"; // Return "lotion"
            break;
         default: // Default case for switch statement
            returnValue = "coin"; // Return "coin"
            break;
      } // End of switch statement
      
      return returnValue; // Returns returnValue
   } // End of generateObjectNoun method
   
   public static void generateFirstSentence(String sentence) { // Creates method to generate the first sentence chosen
      System.out.print(sentence + " "); // Prints the first sentence
   } // End of generateFirstSentence method
   
   public static void generateActionSentence(Random randomGenerator, String subjectNoun) { // Creates method to generate an action sentence
      String sentence = "", lastAdjective, currentAdjective; // Declares string variables for later use in code
      int sentenceStyle = randomGenerator.nextInt(2) + 1; // Sets sentenceStyle to either 1 or 2 to determine the sentence style to use
      
      if (sentenceStyle == 1) { // If sentenceStyle is 1
         sentence += "This " + subjectNoun + " " + generateVerb(randomGenerator) + " a "; // Starts off the sentence
         lastAdjective = ""; // Sets lastAdjective to hold the first adjective so we can make sure the second adjective (if it exists) doesn't equal the first adjective
         for (int i = 0; i < randomGenerator.nextInt(3); i++) { // Creates a for loop that iterates 0, 1, or 2 times
            do { currentAdjective = generateAdjective(randomGenerator); } // Keeps generating new adjectives...
            while (currentAdjective.equals(lastAdjective)); // until it doesn't equal the first adjective (only possible on 2nd iteration)
            sentence += currentAdjective + " "; // Concatenates the adjectives onto the current sentence
            lastAdjective = currentAdjective; // Set lastAdjective to the last adjective we used
         } // End of for loop
         sentence += generateObjectNoun(randomGenerator) + ". "; // Finishes the sentence
      } // End of if statement
      else { // If sentenceStyle is 2
         sentence += "It " + generateVerb(randomGenerator) + " a "; // Starts off the sentence
         lastAdjective = ""; // Sets lastAdjective to hold the first adjective so we can make sure the second adjective (if it exists) doesn't equal the first adjective
         for (int i = 0; i < randomGenerator.nextInt(3); i++) { // Creates a for loop that iterates 0, 1, or 2 times
            do { currentAdjective = generateAdjective(randomGenerator); } // Keeps generating new adjectives...
            while (currentAdjective.equals(lastAdjective)); // until it doesn't equal the first adjective (only possible on 2nd iteration)
            sentence += currentAdjective + " "; // Concatenates the adjectives onto the current sentence
            lastAdjective = currentAdjective; // Set lastAdjective to the last adjective we used
         } // End of for loop
         sentence += generateObjectNoun(randomGenerator) + ". "; // Finishes the sentence
      } // End of if statement
      
      System.out.print(sentence); // Prints out the generated action sentence
   } // End of generateActionSentence method
   
   public static void generateConclusionSentence(Random randomGenerator, String subjectNoun) { // Creates a method to generate a conclusion sentence
      System.out.println("That " + subjectNoun + " " + generateVerb(randomGenerator) + " those " + generateObjectNoun(randomGenerator) + "s!"); // Prints out a sentence generated in the style of a conclusion sentence
   } // End of generateConclusionSentence method

   public static void generateParagraph(Random randomGenerator, String firstSentence, String subjectNoun) { // Creates a method to generate the entire paragraph
      generateFirstSentence(firstSentence); // Generates the first sentence created in phase 1
      for (int i = 0; i < randomGenerator.nextInt(3) + 2; i++) { // Creates a for loop that iterates 2 - 4 times
         generateActionSentence(randomGenerator, subjectNoun); // Generates an action sentence
      } // End of for loop
      generateConclusionSentence(randomGenerator, subjectNoun); // Generates the conclusion sentence
   } // End of generateParagraph method
} // End of class