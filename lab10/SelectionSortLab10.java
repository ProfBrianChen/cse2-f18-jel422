// Jason Lee | CSE 002 Section 110 | 12/7/18 | Lab 10: Experimenting with Sorting

/* Purpose of code:
  To develop algorithms to perform selection and insertion sort on arrays
 */

import java.util.Arrays; // Imports the arrays class for future use in code

public class SelectionSortLab10 { // Creates a class for the Java program
   public static void main(String[] args) { // Main method required in every Java program
      int[] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9}; // Declares and assigns values for an array with 9 elements
      int[] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1}; // Declares and assigns values for an array with 9 reverse elements
      
      int iterBest = selectionSort(myArrayBest); // Sorts myArrayBest through selectionSort()
      System.out.println("The total number of operations performed on the sorted array: " + iterBest); // Prints the number of operations performed on the best array
      System.out.println(); // Prints a new line (aesthetic)
      
      int iterWorst = selectionSort(myArrayWorst); // Sorts myArrayWorst through selectionSort()
      System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst); // Prints the number of operations performed on the worst array
   } // End of main() method
   
   public static int selectionSort(int[] list) { // Start of selectionSort() method
      System.out.println(Arrays.toString(list)); // Prints the elements of array
      int iterations = 0; // Counter for iterations
      
      for (int i = 0; i < list.length - 1; i++) { // Start of outer for loop
         iterations++; // Updates the iterations counter
         int currentMin = list[i]; // Sets the minimum variable to the ith element in the list
         int currentMinIndex = i; // Sets the current minimum index to i
         for (int j = i + 1; j < list.length; j++) { // Start of inner for loop
            iterations++; // Updates the iterations counter (for each comparison)
            if (list[j] < currentMin) { // If the jth element of list is less than the current ith value...
               currentMin = list[j]; // Set currentMin to the jth element of the list
               currentMinIndex = j; // Set currentMinIndex to the index of j
            } // End of if statement
         } // End of for loop
         if (currentMinIndex != i) { // If the minimum index we found doesn't equal i...
            list[currentMinIndex] = list[i]; // Set the value at the currentMinIndex to the value at i (swapping the two values)
            list[i] = currentMin; // Set the value at i to the currentMin value (swapping the two values)
            System.out.println(Arrays.toString(list)); // Prints the new array because a change was made
         } // End of if statement
      } // End of for loop
      
      return iterations; // Returns the amount of operations performed
   } // End of selectionSort() method
} // End of class