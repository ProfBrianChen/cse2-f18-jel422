// Jason Lee | CSE 002 Section 110 | 12/7/18 | Lab 10: Experimenting with Sorting

/* Purpose of code:
  To develop algorithms to perform selection and insertion sort on arrays
 */

import java.util.Arrays; // Imports the arrays class for future use in code

public class InsertionSortLab10 { // Creates a class for the Java program
   public static void main(String[] args) { // Main method required in every Java program
      int[] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9}; // Declares and assigns values for the best array
      int[] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1}; // Declares and assigns values for the worst array
      
      int iterBest = insertionSort(myArrayBest); // Performs insertionSort() on myArrayBest
      System.out.println("The total number of operations performed on the sorted array: " + iterBest); // Prints the amount of operations performed on the best array
      System.out.println(); // Prints a new line (aesthetic)
         
      int iterWorst = insertionSort(myArrayWorst); // Performs insertionSort() on myArrayWorst
      System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst); // Prints the amount of operations performed on the worst array
   } // End of main method // End of main method

   public static int insertionSort(int[] list) { // Start of insertionSort() method
      System.out.println(Arrays.toString(list)); // Prints the elements of the array
      int iterations = 0, i, j; // Declares and sets 0 to the iterations counter

      for (i = 1; i < list.length; i++) { // Creates a for loop that runs (list.length - 1) times
         iterations++; // Updates the iterations counter
         for(j = i; j > 0; j--) { // Creates a for loop that runs i times (until broken)
            if (list[j] < list[j - 1]) { // If the jth element is less than the element behind it...
               iterations++; // Updates the iterations counter
               int temp = list[j]; // Sets temp to the jth element of list (swapping)
               list[j] = list[j - 1]; // Sets list[j] to the element behind it (swapping)
               list[j - 1] = temp; // Sets the element behind to list[j]'s originaln value (swapping)
            } // End of if statement
            else { // IF the jth element is equal to or more than the element behind it...
               break; // Break out of the inner for loop (this element is properly sorted)
            } // End of else statement
         } // End of for loop
         if (j != i) { // If there was at least one swap...
            System.out.println(Arrays.toString(list)); // Prints the elements of the array (one more element is properly sorted)
         } // End of if statement
      } // End of for loop
      
      return iterations; // Returns the number of operations performed on list
   } // End of insertionSort() method
} // End of class