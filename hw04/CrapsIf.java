// Jason Lee | CSE 002 Section 110 | 9/25/18 | Homework 04: Craps If

import java.util.Scanner; // Imports the Scanner class for future use in code

public class CrapsIf { // Creates a class for the Java program
	public static void main(String[] args) { // Main method required for every Java program
		Scanner myScanner = new Scanner(System.in); // Declares an instance of a Scanner object that takes input
		int rollOne = 0; // Defines rollOne and sets a value for it
		int rollTwo = 0; // Defines rollTwo and sets a value for it
		int rollSum; // Defines a variable for the sum of both dice rolls later in the code
		
		System.out.print("Would you like to randomly cast dice rolls or select your own dice rolls? Enter 1 for random dice rolls or 2 to select your own dice rolls: ");
			// Prompts the user to input 1 for randomly casted dice or 2 to input their own dice
		int selectionMethod = myScanner.nextInt();
			// Sets the next integer the user inputs (which should be the selection method) to the variable selectionMethod
		
		if ((selectionMethod == 1) || (selectionMethod == 2)) { // Verifies the user actually inputted a valid selection method (1 or 2)
			if (selectionMethod == 1) { // User said they wanted randomly-cast dice rolls
				rollOne = (int) (Math.random() * 6) + 1; // Casts a random dice roll 1-6 to rollOne
				rollTwo = (int) (Math.random() * 6) + 1; // Casts a random dice roll 1-6 to rollTwo
			}
			else if (selectionMethod == 2) { // User said they wanted to input their own dice rolls
				System.out.print("Input your first dice roll: "); // Prompts user to input their first roll
				rollOne = myScanner.nextInt(); // Sets the user's input to rollOne
				System.out.print("Input your second dice roll: "); // Prompts user to input their second roll
				rollTwo = myScanner.nextInt(); // Sets the user's input to rollTwo
			}
			if ((1 <= rollOne) && (rollOne <= 6) && (1 <= rollTwo) && (rollTwo <= 6)) { // Verifies both dice rolls are integers 1-6 inclusive
				rollSum = rollOne + rollTwo; // Sets rollSum to the sum of both rolls
				if (rollSum == 2) { // If both numbers add to 2
					System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Snake Eyes");
					// Displays the outcome of the dice rolls to the user 
				}
				else if (rollSum == 3) { // If both numbers add to 3
					System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Ace Deuce");
					// Displays the outcome of the dice rolls to the user
				}
				else if (rollSum == 4) { // If both numbers add to 4
					if (rollOne == rollTwo) { // If rolls one and two are the same
						System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Hard Four");
						// Displays the outcome of the dice rolls to the user
					}
					else { // If rolls one and two are not the same
						System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Easy Four");
						// Displays the outcome of the dice rolls to the user
					}
				}
				else if (rollSum == 5) { // If both numbers add to 5
					System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Fever Five");
					// Displays the outcome of the dice rolls to the user
				}
				else if (rollSum == 6) { // If both numbers add to 6
					if (rollOne == rollTwo) { // If rolls one and two are the same
						System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Hard Six");
						// Displays the outcome of the dice rolls to the user
					}
					else { // If rolls one and two are not the same
						System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Easy Six");
						// Displays the outcome of the dice rolls to the user
					}
				}
				else if (rollSum == 7) { // If both numbers add to 7
					System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Seven Out");
					// Displays the outcome of the dice rolls to the user
				}
				else if (rollSum == 8) { // If both numbers add to 8
					if (rollOne == rollTwo) { // If rolls one and two are the same
						System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Hard Eight");
						// Displays the outcome of the dice rolls to the user
					}
					else { // If rolls one and two are not the same
						System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Easy Eight");
						// Displays the outcome of the dice rolls to the user
					}
				}
				else if (rollSum == 9) { // If both numbers add to 9
					System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Nine");
					// Displays the outcome of the dice rolls to the user
				}
				else if (rollSum == 10) { // If both numbers add to 10
					if (rollOne == rollTwo) { // If rolls one and two are the same
						System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Hard Ten");
						// Displays the outcome of the dice rolls to the user
					}
					else { // If rolls one and two are not the same
						System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Easy Ten");
						// Displays the outcome of the dice rolls to the user
					}
				}
				else if (rollSum == 11) { // If both numbers add to 11
					System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Yo-leven");
					// Displays the outcome of the dice rolls to the user
				}
				else if (rollSum == 12) { // If both numbers add to 12
					System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Boxcars");
					// Displays the outcome of the dice rolls to the user
				}
			}
			else { // If one or both of the dice rolls were not an integer between 1-6 inclusive
				System.out.println("Either one or both dice rolls were invalid (i.e. one or both dice rolls were not 1 - 6 inclusive).");
			}
		}
		else { // If the user did not input a valid seleciton method (i.e. did not input 1 or 2), this lets them know they made an error
			System.out.println("An invalid selection method was chosen (i.e. neither 1 or 2 was inputted).");
		}
	} // End of main method
} // End of class