// Jason Lee | CSE 002 Section 110 | 9/25/18 | Homework 04: Craps Switch

import java.util.Scanner; // Imports the Scanner class for future use in code

public class CrapsSwitch { // Creates a class for the Java program
	public static void main(String[] args) { // Main method required for every Java program
		Scanner myScanner = new Scanner(System.in); // Declares an instance of a Scanner object that takes input
		int rollOne = 0; // Defines rollOne and sets a value for it
		int rollTwo = 0; // Defines rollTwo and sets a value for it
		int rollsSum; // Defines a variable for the sum of the two rolls (used later in code)
		int rollsSelected; // Defines a variable to determine if the dice rolls were selected (acts as a boolean, but booleans cannot be used in switch cases so an int is used)
		
		System.out.print("Would you like to randomly cast dice rolls or select your own dice rolls? Enter 1 for random dice rolls or 2 to select your own dice rolls: ");
			// Prompts the user to input 1 for randomly casted dice or 2 to input their own dice
		int selectionMethod = myScanner.nextInt();
			// Sets the next integer the user inputs (which should be the selection method) to the variable selectionMethod
		
		switch (selectionMethod) { // Selects the dice rolls based on what selection method the user chose
			case 1: // User said they wanted randomly-cast dice rolls
				rollOne = (int) (Math.random() * 6) + 1; // Casts a random dice roll 1-6 to rollOne
				rollTwo = (int) (Math.random() * 6) + 1; // Casts a random dice roll 1-6 to rollTwo
				rollsSelected = 1; // Rolls were selected
				break;
			case 2: // User said they wanted to input their own dice rolls
				System.out.print("Input your first dice roll: "); // Prompts user to input their first roll
				rollOne = myScanner.nextInt(); // Sets the user's input to rollOne
				System.out.print("Input your second dice roll: "); // Prompts user to input their second roll
				rollTwo = myScanner.nextInt(); // Sets the user's input to rollTwo
				rollsSelected = 1; // Rolls were selected
				break;
			default: // If the user did not input a valid seleciton method (i.e. did not input 1 or 2), this lets them know they made an error
				System.out.println("An invalid selection method was chosen (i.e. neither 1 or 2 was inputted)."); // User knows they did not put in 1 or 2
				rollsSelected = 0; // Rolls were NOT selected (causes the next switch statement in code to instantly default)
				break;
		}
		
		switch (rollsSelected) { // Beginning of calculation to determine slang for dice rolls; if rolls were not selected properly, this code will default
			case 1: // If the rolls were selected properly (if the user properly picked random or selected dice rolls)
				rollsSum = rollOne + rollTwo; // Sets rollSum to the sum of the two rolls (used to calculate the slang terminology)
				switch (rollsSum) { // Statements for each possible sum (2-12). If sum is not 2-12, at least one dice roll was not 1-6.
					case 2: // If sum is 2
						switch (rollOne) { // Check the value of roll one
							case 1: // If roll one was one, then roll two must have been one to get a sum of 2
								System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Snake Eyes"); // Snake Eyes display
								break;
							default: // If roll one was not one, then roll two must not have been 1-6, so error statement shows
								System.out.println("Either one or both dice rolls were invalid (i.e. one or both dice rolls were not 1 - 6 inclusive).");
								break;
						}
						break;
					case 3: // If sum is 3
							switch (rollOne) { // Check the value of roll one
								case 1: // If roll one was one, then roll two must have been two
								case 2: // If roll one was two, then roll two must have been one
									System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Ace Deuce"); // Ace Deuce display
									break;
								default: // If roll one was not one or two, then roll two must not have been 1-6, so error statement shows
									System.out.println("Either one or both dice rolls were invalid (i.e. one or both dice rolls were not 1 - 6 inclusive).");
									break;
							}
						break;
					case 4: // If sum is 4
						switch (rollOne) { // Check the value of roll one
							case 2: // If roll one was two, then roll two must have been two
								System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Hard Four"); // Hard Four display
								break;
							case 1: // If roll one was one or three, then roll two must have been three or one
							case 3:
								System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Easy Four"); // Easy Four display
								break;
							default: // If roll one was not one two or three, then roll two must not have been 1-6, so error statement shows
								System.out.println("Either one or both dice rolls were invalid (i.e. one or both dice rolls were not 1 - 6 inclusive).");
								break;
						}
						break;
					case 5: // If sum is 5
						switch (rollOne) { // Check the value of roll one
							case 1: // If roll one was 1, 2, 3, or 4, then roll two must have been 4, 3, 2, or 1
							case 2:
							case 3:
							case 4:
								System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Fever Five"); // Fever Five display
								break;
							default: // If roll one was not 1, 2, 3, or 4, then roll two must not have been 1-6, so error statement shows
								System.out.println("Either one or both dice rolls were invalid (i.e. one or both dice rolls were not 1 - 6 inclusive).");
								break;
						}
						break;
					case 6: // If sum is 6
						switch (rollOne) { // Check the value of roll one
							case 3: // If roll one was three, then roll two must have been three
								System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Hard Six"); // Hard Six display
								break;
							case 1: // If roll one was 1, 2, 4, or 5, then roll two must have been 5, 4, 2, or 1
							case 2:
							case 4:
							case 5:
								System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Easy Six"); // Easy Six display
								break;
							default: // If roll one was not 1, 2, 3, 4, or 5, then roll two must not have been 1-6, so error statement shows
								System.out.println("Either one or both dice rolls were invalid (i.e. one or both dice rolls were not 1 - 6 inclusive).");
								break;
						}
						break;
					case 7: // If sum is 7
						switch (rollOne) { // Check the value of roll one
							case 1: // If roll one was 1, 2, 3, 4, 5, or 6, then roll two must have been 6, 5, 4, 3, 2, or 1
							case 2:
							case 3:
							case 4:
							case 5:
							case 6:
								System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Seven Out"); // Seven Out display
								break;
							default: // If roll one was not 1, 2, 3, 4, 5, or 6, then roll two must not have been 1-6, so error statement shows
								System.out.println("Either one or both dice rolls were invalid (i.e. one or both dice rolls were not 1 - 6 inclusive).");
								break;
						}
						break;
					case 8: // If sum is 8
						switch (rollOne) { // Check the value of roll one
							case 4: // If roll one was four, then roll two must have been four
								System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Hard Eight"); // Hard Eight display
								break;
							case 2: // If roll one was 2, 3, 5, or 6, then roll two must have been 6, 5, 3, or 2
							case 3:
							case 5:
							case 6:
								System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Easy Eight"); // Easy Eight display
								break;
							default: // If roll one was not 2, 3, 4, 5, or 6, then roll two must not have been 1-6, so error statement shows
								System.out.println("Either one or both dice rolls were invalid (i.e. one or both dice rolls were not 1 - 6 inclusive).");
								break;
						}
						break;
					case 9: // If sum is 9
						switch (rollOne) { // Check the value of roll one
							case 3: // If roll one was 3, 4, 5, or 6, then roll two must have been 6, 5, 4, or 3
							case 4:
							case 5:
							case 6:
								System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Nine"); // Nine display
								break;
							default: // If roll one was not 3, 4, 5, or 6, then roll two must not have been 1-6, so error statement shows
								System.out.println("Either one or both dice rolls were invalid (i.e. one or both dice rolls were not 1 - 6 inclusive).");
								break;
						}
						break;
					case 10: // If sum is 10
						switch (rollOne) { // Check the value of roll one
							case 5: // If roll one was five, then roll two must have been five
								System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Hard Ten"); // Hard Ten display
								break;
							case 4: // If roll one was 4 or 6, then roll two must have been 6 or 4
							case 6:
								System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Easy Ten"); // Easy Ten display
								break;
							default: // If roll one was not 4 or 6, then roll two must not have been 1-6, so error statement shows
								System.out.println("Either one or both dice rolls were invalid (i.e. one or both dice rolls were not 1 - 6 inclusive).");
								break;
						}
						break;
					case 11: // If sum is 11
						switch (rollOne) { // Check the value of roll one
							case 5: // If roll one was 5 or 6, then roll two must have been 6 or 5
							case 6:
								System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Yo-leven"); // Yo-leven display
								break;
							default: // If roll one was not 5 or 6, then roll two must not have been 1-6, so error statement shows
								System.out.println("Either one or both dice rolls were invalid (i.e. one or both dice rolls were not 1 - 6 inclusive).");
								break;
						}
						break;
					case 12: // If sum is 12
						switch (rollOne) { // Check the value of roll one
							case 6: // If roll one was 6, then roll two must have been 6
								System.out.println("Dice rolls " + rollOne + " and " + rollTwo + " produce: Boxcars");
								break;
							default: // If roll one was not 6, then roll two must not have been 1-6, so error statement shows
								System.out.println("Either one or both dice rolls were invalid (i.e. one or both dice rolls were not 1 - 6 inclusive).");
								break;
						}
						break;
					default: // If sum is not 2-12, then at least one roll was not 1-6, so error statement shows
						System.out.println("Either one or both dice rolls were invalid (i.e. one or both dice rolls were not 1 - 6 inclusive).");
						break;
				} // End of rollSum switch statement
		} // End of rollsSelected switch statement
	} // End of main method
} // End of class