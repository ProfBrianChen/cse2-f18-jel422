// Jason Lee | CSE 002 Section 110 | 9/14/18 | Lab 03: Scanner

/* Purpose of code:
Obtains from the user:
The cost of a dinner check
The percentage tip they wish to pay
The number of ways the check will be split

Prints: How much each person in the group needs to spend in order to pay the check. */

import java.util.Scanner; // Imports the Scanner class for future use in code

public class Check { // Main method required for every Java program
	public static void main(String[] args) { // Creates a class for the Java program
		Scanner myScanner = new Scanner(System.in);
			// Declares an instance of a Scanner object that takes input
		System.out.print("Enter the original cost of the check in the form xx.xx: ");
			// Prompts the user to input the cost of their dinner check
		double checkCost = myScanner.nextDouble();
			// Sets the next double the user inputs (which should be the cost of the dinner check) to the variable checkCost
		System.out.print("Enter the percentage tip that you wish to pay as a whole number in the form xx: ");
			// Prompts the user to input the percentage they wish to tip
		double tipPercent = myScanner.nextDouble();
			// Sets the next double the user inputs (which should be the percentage they wish to tip) to the variable tipPercent
		tipPercent /= 100;
			// Converts the value for the variable tipPercent into a decimal for future use in code
		System.out.print("Enter the number of people who went out to dinner: ");
			// Prompts the user to input the nunmber of people who went out to dinner
		int numPeople = myScanner.nextInt();
			// Sets the next double the user inputs (which should be the number of people who went out) to the variable numPeople
		
		double totalCost; // Declares a variable for the total cost including the tip
		double costPerPerson; // Declares a variable for the cost each person must pay
		int wholeNum, tenthsPlace, hundredthsPlace; // Declares variables for the value of each digit for the cost
		
		totalCost = checkCost * (1 + tipPercent);
			// Calculates the total cost (including tip) of the dinner and assigns it to totalCost
		costPerPerson = totalCost / numPeople;
			// Calculates the cost each person must pay for the dinner and assings it to costPerPerson
		wholeNum = (int) costPerPerson;
			// Truncates the decimals in the variable costPerPerson (giving us the whole number amount) and assigns it to wholeNum
		tenthsPlace = (int) (costPerPerson * 10) % 10;
			// Calculates the tenths place of the variable costPerPerson by typecasting and modular arithmetic
		hundredthsPlace = (int) (costPerPerson * 100) % 10;
			// Calculates the hundredths place of the variable costPerPerson by typecasting and modular arithmetic
		System.out.println("Each person in the group owes $" + wholeNum + "." + tenthsPlace + "" + hundredthsPlace + ".");
			// Prints the amount that each person owes for the dinner to the output
	} // End of class
} // End of main method