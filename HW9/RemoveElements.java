// Jason Lee | CSE 002 Section 110 | 11/27/18 | Homework 09: Fun With Searching

import java.util.Scanner; // Imports the Scanner class for future use in code
import java.util.Random; // Imports the Random class for future use in code

public class RemoveElements { // Creates a class for the Java program
   public static void main(String[] args) { // Main method required for every Java program
      Scanner scan = new Scanner(System.in); // Creates a scanner object to take input from the user
      int[] num = new int[10]; // Creates an array with size 10
      int[] newArray1; // Declares a variable for a new array
      int[] newArray2; // Declares a variable for a new array
      int index, target; // Declares variables for the arguments of two of the methods
      String answer = ""; // Declares a variable to see if the user wants to run the program again
      boolean intCheck; // Used to check if the user's input is an integer

      do { // Start of do-while loop
         System.out.println("Random input 10 integers [0-9]"); // States 10 integers ranging from 0 - 9 are being entered into num
         num = randomInput(); // Uses randomInput() to enter 10 random integers into num
         String out = "The original array is: "; // Prints the array
         out += listArray(num); // Appends the array onto "out"
         System.out.println(out); // Prints the full statement
         
         System.out.print("Enter the index: "); // Prompts the user to input an index for the delete() method
         while (true) { // Repeats infinitely until broken out of
            intCheck = scan.hasNextInt(); // Checks if the value the user inputted is an integer
            if (intCheck) { // If the user entered an integer
               index = scan.nextInt(); // Sets index to that integer
               if ((index >= 0) && (index <= 9)) { // If the integer is within the range 0 - 9 inclusive...
                  break; // Break out of the while loop
               } // End of if statement
               else { // If an integer out of the range 0 - 9 was entered...
                  System.out.print("A number out of the range 0 - 9 was entered. Try again: "); // Prompt the user saying they put an integer out of range
               } // End of else statement
            } // End of if statement
            else { // If the user's input was not an integer...
               scan.next(); // Ignore the value the user inputted
               System.out.print("An integer was not entered. Try again: "); // Prompt the user saying they did not input an integer
            } // End of else statement
         } // End of while loop
         newArray1 = delete(num, index); // Sets newArray1 to the original array but with the index removed
         String out1 = "The output array is: "; // Prints the new array
         out1 += listArray(newArray1); // Return a string of the form "{2, 3, -9}"  
         System.out.println(out1); // Prints the full statement
         
         System.out.print("Enter the target value: "); // Prompts the user to enter a target value for the remove() method
         while (true) { // Repeats infinitely until broken out of
            intCheck = scan.hasNextInt(); // Checks if the value the user inputted is an integer
            if (intCheck) { // If the user entered an integer
               target = scan.nextInt(); // Sets target to that integer
               break; // Break out of the while loop
            } // End of if statement
            else { // If the user's input was not an integer...
               scan.next(); // Ignore the value the user inputted
               System.out.print("An integer was not entered. Try again: "); // Prompt the user saying they did not input an integer
            } // End of else statement
         } // End of while loop
         newArray2 = remove(num, target); // Sets newArray2 to the original array but with all elements that are target removed
         String out2 = "The output array is: "; // Prints the new array
         out2 += listArray(newArray2); // Return a string of the form "{2, 3, -9}"  
         System.out.println(out2); // Prints the full statement

         System.out.print("Go again? Enter 'y' or 'Y', anything else to quit: "); // Prompts the user if they would like to run the program again
         answer = scan.next(); // Takes input from the user for their answer
      } while (answer.equals("Y") || answer.equals("y")); // End of do-while loop (repeat while the user keeps entering "y" or "Y"
   } // End of main method
   
   public static String listArray(int[] num){ // Start of listArray() method
      String out = "{"; // Starts the string being returned
      
      for (int j = 0; j < num.length; j++) { // Repeats the length of num times
         if (j > 0) { // If we are past the first iteration...
            out += ", "; // Append a comma and space onto the string
         } // End of if statement
         out += num[j]; // Append the element at index j to the string
      } // End of for loop
      
      out += "}"; // Ends the string being returned
      
      return out; // Returns the string
   } // End of listArray() method

   public static int[] randomInput() { // Start of randomInput() method
      Random randGen = new Random(); // Creates a random number generator
      int[] returnValue = new int[10]; // Creates an array with size 10
      
      for (int i = 0; i < returnValue.length; i++) { // Repeats for however long returnValue is
         returnValue[i] = randGen.nextInt(10); // Sets the element at index i to a random integer from 0 - 9 inclusive
      } // End of for loop
      
      return returnValue; // Returns returnValue
   } // End of randomInput() method
   
   public static int[] delete(int[] list, int pos) { // Start of delete() method
      int[] returnValue = new int[list.length - 1]; // Creates an array with size that is one less than list's size
      
      for (int i = 0; i < list.length; i++) { // Repeats list.length times
         if (i < pos) { // If the iteration we are on is less than the desired position to delete...
            returnValue[i] = list[i]; // Set the ith element of returnValue to the ith element of list
         } // End of if statement
         else if (i > pos) { // If the iteration we are on is more than the desired position to delete...
            returnValue[i - 1] = list[i]; // Set the (i - 1)th element of returnValue to the ith element of list (to account for the missing element)
         } // End of else-if statement
      } // End of for loop
      
      return returnValue; // Returns returnValue
   } // End of delete() method
   
   public static int[] remove(int[] list, int target) { // Start of remove() method
      int[] returnValue; // Declares a variable for returnValue (we don't know the desired size yet)
      int returnLength = list.length, targetCounter = 0; // Declares variables for counters for the length of the new array and the amount of elements with the target value
      
      for (int i = 0; i < list.length; i++) { // Repeats list.length times
         if (list[i] == target) { // If the ith element of list is the target value
            returnLength--; // Decreaes the returnLength counter by 1
         } // End of if statement
      } // End of for loop
      
      returnValue = new int[returnLength]; // Assigns returnValue with a new array with size returnLength (the desired size without the target value elements)
      
      for (int i = 0; i < list.length; i++) { // Repeats list.length times
         if (list[i] != target) { // If the ith element of list isn't the same as target
            returnValue[i - targetCounter] = list[i]; // Assign the (i - targetCounter)th element of returnValue with the ith element of list (to account for the lost elements)
         } // End of if statement
         else { // If the ith element of list IS the same as target
            targetCounter++; // Increase the target counter by 1
         } // End of else statement
      } // End of for loop
      
      return returnValue; // Returns the returnValue
   } // End of remove() method
} // End of class