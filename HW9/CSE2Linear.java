// Jason Lee | CSE 002 Section 110 | 11/27/18 | Homework 09: Fun With Searching

import java.util.Scanner; // Imports the Scanner class for future use in code
import java.util.Random; // Imports the Random class for future use in code

public class CSE2Linear { // Creates a class for the Java program
   public static void main(String[] args) { // Main method required for every Java program
      int[] grades = new int[15]; // Creates an array with length 15 to hold the values for the grades entered by the user
      Scanner myScanner = new Scanner(System.in); // Creates a scanner object to receive input for the user's input
      boolean intCheck; // Declares a variable to hold the value for if the user's input is an int or not
      int lastInt, gradeSearch, searchResult; // Declares variables to hold the value for the last integer the user entered, the grade to search for, and the results of the last search
      
      for (int i = 0; i < grades.length; i++) { // Creates a loop that repeats the length of the array times
         System.out.print("Enter final grade number " + (i + 1) + ": "); // Prompts the user to enter the ith grade (will be i + 1 because i starts at 0)
         while (true) { // Repeats infinitely until broken out of
            intCheck = myScanner.hasNextInt(); // Checks if the value the user inputted is an integer
            if (intCheck) { // If the user inputted an integer
               lastInt = myScanner.nextInt(); // Sets lastInt to that integer
               if ((lastInt >= 0) && (lastInt <= 100)) { // If the integer is within the range 0 - 100 inclusive...
                  if ((i == 0) || (lastInt >= grades[i - 1])) { // If this is the first iteration or if the grade entered is greater than or equal to the last integer entered...
                     break; // Break out of the while loop
                  } // End of if statement
                  else { // If the integer entered is less than the last integer entered...
                     System.out.print("A number less than the last final grade was entered. Try again: "); // Prompt the user saying they put an integer less than their last value
                  } // End of else statement
               } // End of if statement
               else { // If an integer out of the range 0 - 100 was entered...
                  System.out.print("A number out of the range 0 - 100 was entered. Try again: "); // Prompt the user saying they put an integer out of range
               } // End of else statement
            } // End of if statement
            else { // If the user's input was not an integer...
               myScanner.next(); // Ignore the value the user inputted
               System.out.print("An integer was not entered. Try again: "); // Prompt the user saying they did not input an integer
            } // End of else statement
         } // End of while loop
         grades[i] = lastInt; // Sets the ith value of grades to the last integer (which now fits the criteria since we are out of the while loop)
      } // End of for loop
      
      System.out.print("Grades Entered: "); // Begin printing the elements of grades
      print(grades); // Prints the elements of grades
      
      System.out.print("Enter a grade to search for: "); // Prompts the user to enter a grade to search for
      intCheck = myScanner.hasNextInt(); // Checks if the value the user inputted is an integer
      while (!intCheck) { // Repeat while the user doesn't input an integer
         myScanner.next(); // Ignore the value the user inputted
         System.out.print("The input was not an integer. Try again: "); // Tells the user their input was invalid and prompts them to try again
         intCheck = myScanner.hasNextInt(); // Checks if the new value the user inputted is an integer
      }
      gradeSearch = myScanner.nextInt(); // Sets the variable gradeSearch to the integer the user inputted
      
      binarySearch(grades, gradeSearch); // Does a binary search for the user's input
      scramble(grades); // Scrambles the array
      System.out.print("Scrambled: "); // Begin to print a statement saying the array was scrambled
      print(grades); // Print the elements of the array
      
      System.out.print("Enter a grade to search for: "); // Prompts the user to enter a grade to search for
      intCheck = myScanner.hasNextInt(); // Checks if the value the user inputted is an integer
      while (!intCheck) { // Repeat while the user doesn't input an integer
         myScanner.next(); // Ignore the value the user inputted
         System.out.print("The input was not an integer. Try again: "); // Tells the user their input was invalid and prompts them to try again
         intCheck = myScanner.hasNextInt(); // Checks if the new value the user inputted is an integer
      }
      gradeSearch = myScanner.nextInt(); // Sets the variable gradeSearch to the integer the user inputted
      
      linearSearch(grades, gradeSearch); // Does a linear search for the user's input
   } // End of main method
   
   public static void print(int[] list) { // Start of print() method
      for (int i = 0; i < list.length - 1; i++) { // Creates a for loop that runs list.length - 1 times
         System.out.print(list[i] + ", "); // Prints the ith element of list followed by a comma and space
      } // End of for loop
      System.out.println(list[list.length - 1]); // Prints the last element without a comma or space afterwards
   } // End of print() method
   
   public static void scramble(int[] list) { // Start of scramble() method
      int swapIndex, temp; // Declares variables to be used to scramble the array
      Random randGen = new Random(); // Creates a random number generator to generate random numbers
      
      for (int i = 1; i <= 100; i++) { // Repeat 100 times (thoroughly scramble the array)
         swapIndex = randGen.nextInt(14) + 1; // Sets swapIndex to a random value 1 - 14
         temp = list[0]; // Sets temp to the 0th index of list
         list[0] = list[swapIndex]; // Sets the element at 0 to the element at swapIndex
         list[swapIndex] = temp; // Sets the element at swapIndex to temp (the original value of list[0])
      } // End of for loop
   } // End of scramble() method
   
   public static void linearSearch(int[] list, int searchValue) { // Start of linearSearch() method
      int i; // Declares a variable for the amount of iterations
      
      for (i = 1; i <= list.length; i++) { // Creates a for loop that repeats list.length times
         if (list[i - 1] == searchValue) { // If the (i - 1)th element is the same as the value we are searching for...
            if (i != 1) { // If we are not on the first iteration (plural)
               System.out.println(searchValue + " was found in the list with " + i + " iterations."); // Print saying the search value was found (plural)
            } // End of if statement
            else if (i == 1) { // If we are on the first iteration (singular)
               System.out.println(searchValue + " was found in the list with " + i + " iteration."); // Print saying the search value was found (singular)
            } // End of else-if statement
            return; // Ends execution of this method (avoids the print statement on line 102)
         } // End of if statement
      } // End of for loop
      
      System.out.println(searchValue + " was not found in the list with " + (i - 1) + " iterations."); // Print saying the search value was not found
   } // End of linearSearch() method
   
   public static void binarySearch(int[] list, int searchValue) { // Start of binarySearch() method
      int min = 0, mid, max = list.length - 1, i = 0; // Declares variables for the minimum value of our range, middle value of our range, maximum value of our range, and the iteration
      
      while (min <= max) { // While the minimum value is still less than the maximum value
         i++; // Increase the iterations counter by one
         mid = (min + max) / 2; // Sets mid to the middle value of our range using min and max
         if (list[mid] == searchValue) { // If the middle value of our range is the search value...
            if (i != 1) { // If we are not on the first iteration (plural)
               System.out.println(searchValue + " was found in the list with " + i + " iterations."); // Print saying the search value was found (plural)
            } // End of if statement
            else if (i == 1) { // If we are on the first iteration (singular)
               System.out.println(searchValue + " was found in the list with " + i + " iteration."); // Print saying the search value was found (singular)
            } // End of else-if statement
            return; // Ends execution of this method (avoids the print statement on line 128)
         } // End of if statement
         else if (list[mid] < searchValue) { // If the middle value of our array is less than the value we are searching for...
            min = mid + 1; // Set the minimum value to the value to the right of the middle value
         } // End of else-if statement
         else if (list[mid] > searchValue) { // If the middle value of our array is greater than the value we are searching for...
            max = mid - 1; // Set the maximum value to the value to the left of the middle value
         } // End of else-if statement
      } // End of while loop
      
      System.out.println(searchValue + " was not found in the list with " + i + " iterations."); // Print saying the search value was not found
   } // End of binarySearch() method
} // End of class