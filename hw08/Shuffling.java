// Jason Lee | CSE 002 Section 110 | 11/15/18 | Homework 08: Shuffling

import java.util.Scanner; // Imports the scanner class for future use in code

public class Shuffling { // Creates a class for the Java program
   public static void main(String[] args) { // Main method required in every Java program
      Scanner scan = new Scanner(System.in); // Creates a scanner object to take input from the user
      String[] suitNames = {"C", "H", "S", "D"}; // Suits are clubs, hearts, spades, and diamonds
      String[] rankNames = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"}; // Numerical values for the cards
      String[] cards = new String[52]; // Creates an array with length 52 to hold the deck of cards
      String[] hand = new String[5]; // Creates an array with length 5 to hold each hand
      int numCards = 5; // The number of cards in each hand
      int again = 1; // Variable to check if the user wants to draw more cards
      int index = 51; // Variable to hold the value of the last index in the deck of cards
      
      for (int i = 0; i < 52; i++) { // Repeats 52 times
         cards[i] = rankNames[i % 13] + suitNames[i / 13]; // Sets each element in the array to its proper card in an ordered deck of cards
         System.out.print(cards[i] + " "); // Prints the deck of cards
      } // End of for loop
      
      System.out.println(); // Prints new line (aesthetic purposes)
      System.out.println(); // Prints new line (aesthetic purposes)
      shuffle(cards); // Runs the shuffle method which shuffles the deck of cards
      System.out.println("Shuffled"); // Lets the user know a shuffled deck of cards is about to appear
      printArray(cards); // Prints the deck of cards (now shuffled)
      System.out.println(); // Prints new line (aesthetic purposes)
      
      while (again == 1) { // While the user wants to draw more hands
         hand = getHand(cards, index, numCards); // Runs getHand() and sets the hand it returns to the variable "hand"
         System.out.println("Hand"); // Lets the user know a hand is about to appear
         printArray(hand); // Prints the hand that was just created
         System.out.println(); // Prints new line (aesthetic purposes)
         index = index - numCards; // Sets index to the new last card in the deck of cards
         if (index < numCards) { // If there are now less cards in the deck than the number of cards per hand...
            index = 51; // Reset index back to 51
            shuffle(cards); // Shuffles the deck, symbolizing a new deck of cards
            System.out.println("New Shuffled Deck"); // Lets the user know a new shuffled deck of cards is about to appear
            printArray(cards); // Prints the new deck of cards
            System.out.println(); // Prints new line (aesthetic purposes)
         } // End of if statement
         System.out.println("Enter a 1 if you want another hand drawn"); // Prompts the user if they would like to draw another hand
         again = scan.nextInt(); // Takes the integer input from the user
         System.out.println(); // Prints new line (aesthetic purposes)
      } // End of while loop
   } // End of main method
   
   public static void printArray(String[] list) { // Creates a method printArray() that will print the list in its argument
      for (int i = 0; i < list.length; i++) { // Repeats the amount of elements in the list times
         System.out.print(list[i] + " "); // Prints the element at the loop's iteration followed by a space
      } // End of for loop
      System.out.println(); // Prints new line (aesthetic purposes)
   } // End of printArray() method
   
   public static void shuffle(String[] list) { // Creates a method shuffle() that will shuffle the list in its argument
      int swapIndex; // Creates a variable to hold the index of the card being swapped with the card at index 0
      String temp; // Temporary variable needed to swap values
      
      for (int i = 1; i <= 100; i++) { // Repeats 100 times (thoroughly shuffle the deck)
         swapIndex = (int)(Math.random() * (list.length - 1) + 1); // Sets swapIndex to a random value 1 - 52
         temp = list[0]; // Sets temp to the card at index 0
         list[0] = list[swapIndex]; // Sets the card at index 0 to the card at swapIndex
         list[swapIndex] = temp; // Sets the card at swapIndex to the temp value
      } // End of for loop
   } // End of shuffle() method
   
   public static String[] getHand(String[] list, int index, int numCards) { // Creates a method getHand() that will draw a hand from the deck
      String[] result = new String[numCards]; // Creates an array that will be returned to the main method
      
      for (int i = 0; i < numCards; i++) { // Repeats the amount of cards in a hand times
         result[i] = list[index]; // Sets the ith index of the array result to the last card in the deck
         index--; // Decrement index because a card was taken away from the deck
      } // End of for loop
      
      return result; // Returns the hand that was just drawn
   } // End of getHand() method
} // End of class