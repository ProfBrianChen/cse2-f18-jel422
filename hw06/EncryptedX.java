// Jason Lee | CSE 002 Section 110 | 10/23/18 | Homework 06: Encrypted X

import java.util.Scanner; // Imports the Scanner class for future use in code

public class EncryptedX { // Creates a class for the Java program
   public static void main(String[] args) { // Main method required for every Java program
      boolean intCheck; // Declares a boolean variable to check if the user's input is an integer for later use in code
      int gridSize; // Declares an int variable to hold the value for the size of a grid
      Scanner myScanner = new Scanner(System.in); // Creates a scanner instance to receive input from the user
      
      System.out.print("Enter the size of the grid you would like to generate (must be an integer between 0 - 100): "); // Prompts the user for integer input between 0 and 100 for the size of the grid
      intCheck = myScanner.hasNextInt(); // Checks if the value the user inputted is an integer
      while (true) { // Creates an infinite loop
         if (intCheck) { // If the user inputted an integer
            gridSize = myScanner.nextInt(); // Set gridSize to the value the user entered
            if (gridSize < 0 || gridSize > 100) { // If the value is out of the range 0 - 100...
               System.out.print("An invalid value was entered for the size of the grid. Try again: "); // Prompts the user to input another value because their original value was invalid
               intCheck = myScanner.hasNextInt(); // Checks if the value the user inputted is an integer
            } // End of inner if statement
            else { // If the value is in the range 0 - 100...10
               break; // Break out of the while loop and proceed with the rest of the code
            } // End of inner else statement
         } // End of outer if statement
         else { // If the user inputted anything that is not an integer
            myScanner.next(); // Take that value off of the scanner's current value
            System.out.print("An invalid value was entered for the size of the grid. Try again: "); // Prompts the user to input another value because their original value was invalid
            intCheck = myScanner.hasNextInt(); // Checks if the value the user inputted is an integer
         } // End of outer else statement
      } // End of infinite loop
      
      for (int i = 0; i <= gridSize; i++) { // Creates the outer for loop. This loop iterates (gridSize + 1) times in order to create each row of the pattern
         for (int j = 0; j <= gridSize; j++) { // Creates the inner for loop. This loop iterates (gridSize + 1) times in order to create each column of the pattern
            if ((j == i) || (j == gridSize - i)) // If the current character position is either i characters from the left or the right, then...
            { System.out.print(" "); } // Print a space which is part of the X
            else // If the current position is NOT either i characters from the left or the right, then...
            { System.out.print("*"); } // Print a star which creates the outline of the X
         } // End of inner for loop
         System.out.println(); // Moves to the next row of the pattern after the current row has finished (since the inner for loop is finished)
      } // End of outer for loop
   } // End of main method
} // End of class