// Jason Lee | CSE 002 Section 110 | 9/11/18 | Homework 02: Arithmetic Calculations

public class Arithmetic { // Main method required for every Java program
	public static void main(String[] args) { // Creates a class for the Java program
		int numPants = 3; // Declares and assigns a value to the variable for the number of pants bought
		int numShirts = 2; // Declares and assigns a value to the variable for the number of sweatshirts bought
		int numBelts = 1; // Declares and assigns a value to the variable for the number of belts bought
		double pantsPrice = 34.98; // Declares and assigns a value to the variable for the cost of each pair of pantsPrice
		double shirtPrice = 24.99; // Declares and assigns a value to the variable for the cost of each sweatshirt
		double beltPrice = 33.99; // Declares and assigns a value to the variable for the cost of each belt
		double paSalesTax = 0.06; // Declares and assigns a value to the variable for the 6% sales tax in PA
		double totalCostOfPants, totalCostOfShirts, totalCostOfBelts; // Declares total cost of each item as variables
		double taxOnPants, taxOnShirts, taxOnBelts; // Declares the cost of tax on each item as variables
		double subTotal, totalTax, transactionTotal; // Declares the subtotal, total tax, and transaction total as variables
		
		/* For the calculations in lines 21 - 26, the structure "Math.floor((calculation) * 100)/100" is used to truncate the
		result to the hundredths place. The calculation within the parentheses is computed, then multiplied by 100, then rounded
		down by the Math.floor() method, and finally divided by 100. The parentheses around the calculations are there in order
		to improve readability. */
		
		totalCostOfPants = Math.floor((numPants * pantsPrice) * 100)/100; // Calculates the total cost of the pants (without tax)
		totalCostOfShirts = Math.floor((numShirts * shirtPrice) * 100)/100; // Calculates the total cost of the shirts (without tax)
		totalCostOfBelts = Math.floor((numBelts * beltPrice) * 100)/100; // Calculates the total cost of the belts (without tax)
		taxOnPants = Math.floor((totalCostOfPants * paSalesTax) * 100)/100; // Calculates the cost of tax on the pants
		taxOnShirts = Math.floor((totalCostOfShirts * paSalesTax) * 100)/100; // Calculates the cost of tax on the shirts
		taxOnBelts = Math.floor((totalCostOfBelts * paSalesTax) * 100)/100; // Calculates the cost of tax on the belts
		subTotal = totalCostOfPants + totalCostOfShirts + totalCostOfBelts; // Calculates the subtotal for the transaction
		totalTax = taxOnPants + taxOnShirts + taxOnBelts; // Calculates the cost of tax for the transaction
		transactionTotal = subTotal + totalTax; // Calculates the total cost of the transactionTotal
		
		System.out.println("The cost of the " + numPants + " pants is " + totalCostOfPants + "."); // Prints the cost of the pants
		System.out.println("The cost of the " + numShirts + " shirts is " + totalCostOfShirts + "."); // Prints the cost of the shirts
		System.out.println("The cost of the " + numBelts + " belts is " + totalCostOfBelts + "."); // Prints the cost of the belts
		System.out.println("The tax on the pants is " + taxOnPants + "."); // Prints the tax on the pants
		System.out.println("The tax on the shirts is " + taxOnShirts + "."); // Prints the tax on the shirts
		System.out.println("The tax on the belts is " + taxOnBelts + "."); // Prints the tax on the belts
		System.out.println("The subtotal of the transaction is " + subTotal + "."); // Prints the subtotal of the the transaction
		System.out.println("The total tax on the transaction is " + totalTax + "."); // Prints the total tax on the transaction
		System.out.println("The total cost of the transaction is " + transactionTotal + "."); // Prints the transaction total
	} // End of class
} // End of main method