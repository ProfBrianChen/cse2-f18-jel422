// Jason Lee | CSE 002 Section 110 | 10/12/18 | Lab 6: Display Pyramids - Pattern C

/* Purpose of code:
 Prompts the user to input the number of rows they want in a pattern. Then prints the pattern based on their input.
 For example, if the user inputted 6, it would appear like the following:
 1
 2 1
 3 2 1
 4 3 2 1
 5 4 3 2 1
 6 5 4 3 2 1
 */

import java.util.Scanner; // Imports the scanner class for future use in code

public class PatternC { // Creates a class for the Java program
   public static void main(String[] args) { // Main method required for every Java program
      int numRows; // Declares a variable for the user's input
      boolean intCheck; // Declaers a variable to check if the user's input is an integer
      Scanner myScanner = new Scanner(System.in); // Creates a scanner to receive input from the user
      
      System.out.print("Enter the length of the pyramid (number of rows) between 1 and 10: "); // Prompts the user to imput the amount of rows in the pattern
      intCheck = myScanner.hasNextInt(); // Checks if the value the user inputted is an integer
      while (true) { // Creates an infinite loop
         if (intCheck) { // If the user inputted an integer
            numRows = myScanner.nextInt(); // Set numRows to the value the user entered
            if (numRows < 1 || numRows > 10) { // If the value is out of the range 1-10...
               System.out.print("An invalid value was entered for the length of the pyramid. Try again: "); // Prompts the user to input another value because their original value was invalid
               intCheck = myScanner.hasNextInt(); // Checks if the value the user inputted is an integer
            } // End of inner if statement
            else { // If the value is in the range 1-10...
               break; // Break out of the while loop and proceed with the rest of the code
            } // End of inner else statement
         } // End of outer if statement
         else { // If the user inputted anything that is not an integer
            myScanner.next(); // Take that value off of the scanner's current value
            System.out.print("An invalid value was entered for the length of the pyramid. Try again: "); // Prompts the user to input another value because their original value was invalid
            intCheck = myScanner.hasNextInt(); // Checks if the value the user inputted is an integer
         } // End of outer else statement
      } // End of infinite loop
      
      for (int i = 1; i <= numRows; i++) { // Creates a for loop that iterates numRows times. i starts at one and increases by one at the start of each iteration
         for (int j = i; j >= 1; j--) { // Creates a for loop that iterates j*i times per line. j starts at i and decreases by one at the start of each iteration
            System.out.print(j + " "); // Prints the value of j on the current line
         } // End of inner for loop
         System.out.println(); // Goes to the next line to start printing the next row in the pattern
      } // End of outer for loop
   } // End of main method
} // End of class